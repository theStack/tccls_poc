void BubbleSort(int* array, int size)
{
	int swapped;
	int i,j;
	
	for (i=1; i<size; i++) {
		swapped = 0;
		for (j=0; j<size-1; j++) {
			if (array[j] > array[j+1]) {
				int temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
				swapped = 1;
			}	
		}
		if (!swapped)
			break;
	}
}
