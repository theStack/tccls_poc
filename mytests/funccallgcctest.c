extern int foo(int,int,int,int);
extern int bar(void);

int test(int a, int b, int c, int d)
{
    int x, y, z;
    x = a*b*c;
    y = b-c+d;
    z = x+b-a+d;
    z = foo(x+1, y+1, z+1, z+2);
    y = x+z+bar();
    return z+2;
}
