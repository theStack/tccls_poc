extern void destroy_scratch_regs(void);

int regalloc(int a, int b, int c, int d)
{
	int e=a+2, f=b+3, g=c+4, h=d+5, j=b+c, k=c+d, l=a+d;
	int i, result=0;

	for (i=0; i<1000000; i++) {
		if (i%2 == 0)
			e++;
		else
			f++;
		if (i%3 == 0)
			g++;
		else
			h++;
		if (i%4 == 0)
			i++;
		else
			j++;
		k = l ? ++j : ++e;	
		f = ((g + h)*(i << j)-(k+l))<<d+a+b+c+d;
		c--; d++; a--; b++;
		result += f;
		destroy_scratch_regs();
	}

	return result;
}
