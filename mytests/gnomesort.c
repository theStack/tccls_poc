void gnomesort(int *elements, int size)
{
    int pos = 1;

    while (pos < size) {
        if (elements[pos] >= elements[pos-1]) {
            ++pos;
        } else {
            int temp = elements[pos];
            elements[pos] = elements[pos-1];
            elements[pos-1] = temp;
            if (pos > 1)
                --pos;
        }
    }
}
