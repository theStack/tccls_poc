#!/bin/bash
echo Compiling bitcount.c with original TCC...
../../tcc -c bitcount.c > /dev/null
gcc -o bitcount_tccls bitcount.o bitcount_tester.c

echo Compiling bitcount.c with LinearScan TCC...
tcc -c bitcount.c
gcc -o bitcount_tccorig bitcount.o bitcount_tester.c

echo Compiling bitcount.c with gcc -O1...
gcc -O1 -c bitcount.c
gcc -o bitcount_gcc bitcount.o bitcount_tester.c


echo Benchmark Original TCC:
time ./bitcount_tccorig

echo Benchmark LinearScan TCC:
time ./bitcount_tccls

echo Benchmark gcc -O1:
time ./bitcount_gcc
