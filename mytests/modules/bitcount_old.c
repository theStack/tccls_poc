extern unsigned char arg;

int BitCount()
{
    unsigned char number;
    int result;

    number = arg;
    result = 0;

    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;

    //arg = 99; // TEST for STORE on global variables

    return result;
}
