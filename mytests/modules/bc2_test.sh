#!/bin/bash
echo Compiling bitcount.c with original TCC...
tcc -c ../bitcnt_2.c
gcc -o bitcount_tccorig bitcnt_2.o bitcount_tester2.c

echo Compiling bitcount.c with LinearScan TCC...
../../tcc -c ../bitcnt_2.c > /dev/null
gcc -o bitcount_tccls bitcnt_2.o bitcount_tester2.c

#echo Compiling bitcount.c with gcc -O0...
#gcc -O0 -c bitcount.c
#gcc -o bitcount_gcc0 bitcount.o bitcount_tester.c

echo Compiling bitcount.c with gcc -O1...
gcc -O1 -c ../bitcnt_2.c
gcc -o bitcount_gcc1 bitcnt_2.o bitcount_tester2.c


echo -n Benchmark Original TCC:
time ./bitcount_tccorig

echo -n Benchmark LinearScan TCC:
time ./bitcount_tccls

#echo -n Benchmark gcc -O0:
#time ./bitcount_gcc0

echo -n Benchmark gcc -O1:
time ./bitcount_gcc1
