extern unsigned int arg;

int BitCount()
{
    unsigned int number = arg;
    int result = 0;

    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;

    return result;
}
