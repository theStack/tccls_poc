extern int divide(int a, int b);

void test(int a, int b)
{
	int res_gcc, res_tcc;
    res_gcc = a/b;
    printf("%d/%d..... ", a, b); // do function call in-between to destroy r0
    res_tcc = divide(a,b);
	printf("gcc says => %d;;;;  our tcc says => %d;;;;\n", res_gcc, res_tcc);
}

int main()
{
    test(42,6);
	test(666,111);
    test(5,4);
    test(943,120);
    test(123,456);
    test(9,3);
    test(9,-3);
    test(-42,7);
    test(900,20);
    test(2013,167);
    return 0;
}
