int BitCount(unsigned char number)
{
    int result = 0;

    while (number) {
        result += number & 1;
        number >>= 1;
    }
#if 0
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
    result += number & 1;
    number >>= 1;
#endif
    //

    return result;
}
