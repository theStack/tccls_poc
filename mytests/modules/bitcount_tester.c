#include <stdio.h>

int BitCount(unsigned char arg);

int main()
{
    int i, j, result;
    for (j=0; j<1000000; j++) {
    for (i=0; i<256; i++) {
        result = BitCount(i);
        //printf("Number %02d (=0x%02x): %d bits\n", i, i, result);
    }
    }
    return 0;
}
