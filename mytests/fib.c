typedef unsigned int uint;

uint fib(uint n) /* compute n-th fibonacci number for n>=2 */
{
	uint i;
	uint f1=0, f2=1;
	uint fi;

	for (i=2; i<=n; i++) {
		fi = f1 + f2;
		f1 = f2;
		f2 = fi;
	}

	return fi;
}
