int regalloc_test(int a, int b, int n)
{
    int result=0;

    do {
        a += 3;
        b -= n;
        result += (a+b)>>1;
    } while (--n);
    
    return result;
}

/*
void call_regalloc_test(int x1, int x2, int x3, int x4, int x5, int x6)
{
    x5 = 23;
    x6 = 42;
    regalloc_test(3,4,5);
}
*/

/*
void strfunc(char* s) {
    s[3] = '\0';
}
*/

/*
void stringfun()
{
    int x, y;
    strfunc("foobarme, store me right... skldjflasdjflksjdflksjflskdjfklsdjflksdjflskdjflskdjfljdflksdfj");
    x++;
    y--;
}
***=> gen_op() called 2x
*/
