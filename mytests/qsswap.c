int array[16];

//Swap integer values by array indexes
void swap(int a, int b)
{
   int tmp = array[a];
   array[a] = array[b];
   array[b] = tmp;
}
