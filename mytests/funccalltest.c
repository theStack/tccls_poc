void foo(int a, int b, int c, int d, int e, int f)
{
    printf("it was called with the following params: a=%d, b=%d, c=%d, d=%d, e=%d, f=%d\n",
        a+b, b+c, c+d, d+e, e+f, f+a);
    // 3, 5, 7, 9, 11, 7
}

int main()
{
    foo(1, 2, 3, 4, 5, 6);
    //printf("it was called with the following params: a=%d, b=%d, c=%d, d=%d, e=%d, f=%d\n",
    //    10, 20, 30, 40, 50, 60);
    return 0;
}
