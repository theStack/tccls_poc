int main()
{
    int a=3, b=9;
    printf("before: %d, %d\n", a,b);
    swap(&a, &b);
    printf("after: %d, %d\n", a,b);
    return 0;
}
