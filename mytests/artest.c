void foo() {
	int x[501];
    int y;
    int *p1, *p2;

	x[1] = 23;
	x[250] = 40;
	x[458] = 1337;
    y = &x[1];
    p1 = &x[300];
    p2 = &x[400];
    y = *p1 + *p2;
}
