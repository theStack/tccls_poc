int a;
extern void foo();

int main()
{
    a = 0;
    printf("vorher, a==%d\n", a);
    foo();
    printf("nachher, a==%d\n", a);

    return 0;
}
