#ifndef _TCCIR_H
#define _TCCIR_H

/*** Operations of Intermediate Representation ***/
/* TODO: possibly directly assign those values with associated ARM data processing opcodes */
/* TODO: possibly substitute with enums */
/********* begin data-processing instrs. ***********/
#define IR_OP_ADD  0  /* "+" */
#define IR_OP_SUB  1  /* "-" */
#define IR_OP_AND  2  /* "&" */
#define IR_OP_OR   3  /* "|" */
#define IR_OP_XOR  4  /* "^" */
#define IR_OP_SHL  5  /* "<<" unsigned */
#define IR_OP_SHR  6  /* ">>" unsigned */
#define IR_OP_SAR  7  /* ">>" signed */
#define IR_OP_ASS  8  /* ":=" assignment (general) */
#define IR_OP_RSUB 9  /* "-" reverse subtraction */
#define IR_OP_CMP  10 /* compare */
#define IR_OP_TSTZ 11 /* test for zero */
/********* end data-processing instrs. ***********/
#define IR_OP_MUL  12  /* "*" */
/* ATTENTION: fix this mess! */
///* ATTENTION: the following ops are implemented via function calls on ARM! */
//#define IR_OP_SDIV 10 /* "/" signed */
//#define IR_OP_UDIV 11 /* "/" unsigned */
//#define IR_OP_SMOD 12 /* "%" signed */
//#define IR_OP_UMOD 13 /* "%" unsigned */
/* ATTENTION : the following ops need special treatment for codegen, cause of the condition register */

//#define IR_OP_SEQ  14 /* "==" */
//#define IR_OP_SNE  15 /* "!=" */
//#define IR_OP_SLT  16 /* "<" signed */
//#define IR_OP_SGT  17 /* ">" signed */
//#define IR_OP_SLE  18 /* "<=" signed */
//#define IR_OP_SGE  19 /* ">=" signed */
//#define IR_OP_SULT 20 /* "<" unsigned */
//#define IR_OP_SUGT 21 /* ">" unsigned */
//#define IR_OP_SULE 22 /* "<=" unsigned */
//#define IR_OP_SUGE 23 /* ">=" unsigned */
#define IR_OP_SETIF 14

//#define IR_OP_BEQ  24 /* "==" */
//#define IR_OP_BNE  25 /* "!=" */
//#define IR_OP_BLT  26 /* "<" signed */
//#define IR_OP_BGT  27 /* ">" signed */
//#define IR_OP_BLE  28 /* "<=" signed */
//#define IR_OP_BGE  29 /* ">=" signed */
//#define IR_OP_BULT 30 /* "<" unsigned */
//#define IR_OP_BUGT 31 /* ">" unsigned */
//#define IR_OP_BULE 32 /* "<=" unsigned */
//#define IR_OP_BUGE 33 /* ">=" unsigned */
#define IR_OP_BRANCHIF 24
#define IR_OP_JMP 25

/* XXX: TOK_Nset, TOK_Nclear ??? */
#define IR_OP_STORE 34 /* ":=" memory store */
//#define IR_OP_NOT  23 /* "~" */ // should not be needed, is done with XOR 0, topval
#define IR_OP_RETURNVOID   35 /* "return" */
#define IR_OP_RETURNVAL    36 /* "return" */
#define IR_OP_FUNCPARAM    37 /* f(...) arguments */
#define IR_OP_FUNCCALLVOID 38 /* f(...) call */
#define IR_OP_FUNCCALLVAL  39 /* f(...) call */
#define IR_OP_FUNCPARAMVOID 40 /* void argument (needed if return value is available) */

//			"==", "!=", "<S", ">S", "<=S", ">=S", "<U", ">U", "<=U", ">=U", 
//			"==", "!=", "<S", ">S", "<=S", ">=S", "<U", ">U", "<=U", ">=U", 
/* string representations of IR operations (for debugging purposes) */
#define IR_OP_SYMBOLS { "+", "-", "&", "|", "^", "<<U", ">>U", ">>S", ":=", "-(R)", "CMP", "TSTZ", "*", "_", \
            "SETIF", "", "", "", "", "", "", "", "", "", \
            "BRANCHIF", "JMP", "", "", "", "", "", "", "", "", \
			":=", "RETURN", "RETURN X", "PARAM", "CALL", "CALL X", "PARAMVOID"} 

/* TODO: this can be moved to .c, probably, hidden for other modules */
/* Quadruple (TAC) contents */
typedef struct {
  int op;      /* 4 */ 
  SValue src1; /* 32 */
  SValue src2; /* 32 */
  SValue dest; /* 32 */
} Quadruple;   /* ~100 */

#define VREG_LOCALVAR  0
#define VREG_TEMPORARY 1
#define VREG_PARAMETER 2

void IR_Clear(void);
int  IR_AddLocalVar(Sym *s, int stack_offset);
int  IR_GetVregVar(void);
int  IR_GetVregTemp(void);
int  IR_GetVregParam(void);
int  IR_GetVregType(int vreg);
void IR_PreventAllocForVreg(int vreg);
void IR_PreventCoalescing();
void IR_AllowCoalescing();
void IR_StartBasicBlock(void);
void IR_AddFuncParams(CType *func_type);
int  IR_PutOp(int op, SValue *src1, SValue *src2, SValue *dest);
int  IR_GetPC();
void IR_DropReturnValue();
void IR_BackpatchFirst(int t, int target_addr);
void IR_Backpatch(int t, int target_addr);
void IR_BackpatchToHere(int t);
void IR_LivenessAnalysis(void);
void IR_ReplaceVReg(int vreg, int offset, int preg);
void IR_Show(void);
void IR_RegisterAllocationParams();
void IR_GenCode(void);

/********************************* debug functions **************************/
void print_quadruple(Quadruple *quad, int pc);
void print_svalue_short(SValue *sv);
void print_svalue_info(SValue *sv);

extern int within_if;

#endif
