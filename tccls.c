#include "tcc.h"
#include "tccls.h"
#include <stdlib.h>
#include "tccir.h"

//#define SHOW_ALLOCATION_RESULT
#define MIN_REGS 1
#define MAX_REGS 11

int R = 7;

/* register file usage:
    R15 -- PC [Hardware]
    R14 -- LR [Hardware] -- could be used freely, since we save/restore it
    R13 -- SP [Software/EABI]
    R12 -- IP -- can also be used freely
    R11 -- FP [Software?]
    R10-R4 -- freely available
    R3-R0 -- arguments, return value

    ===> R4-R10 (#7) for register allocator
    ===> R12 and R14 are used as spilling registers and for other load/store temporaries
*/
extern int next_vreg_var;
extern uint8_t vreg_ignore_list[];

int register_pool[MAX_REGS];   /* register RX is free if element X is 1 (X is between 0 and R-1) */
unsigned int spill_counter;
unsigned int regs_used; /* how many of the registers are actually used (needed for prolog/epilogue opt.) */

typedef struct {
    unsigned int vreg; /* virtual register */
    unsigned int reg;  /* physical register */
    unsigned int loc;  /* location on stack */
    unsigned int start;
    unsigned int end;
} LiveInterval;

#define MAX_LIVEINTERVALS 20000
LiveInterval LiveIntervals[MAX_LIVEINTERVALS];
int nIntervals;

LiveInterval* ActiveSet[MAX_REGS];
int nActive;

void LS_SetR(int nRegs)
{
    if (nRegs < MIN_REGS || nRegs > MAX_REGS)
        tcc_error("number of available registers (R) must be in range [%d,%d]", MIN_REGS, MAX_REGS);
    R = nRegs;
}

/* get register from pool of free registers */
int RegisterPool_Get()
{
    int i;
    for (i=0; i<R; i++) {
        if (register_pool[i]) {
            register_pool[i] = 0; /* remove register from pool */
            if ((i+1) > regs_used)
                regs_used = (i+1);
            return i+4; /* XXX: change this if register layout changes (e.g. to R0-R10) */
        }
    }
    tcc_error("no free register available for allocator, this should never happen!");
    return ~0;
}

/* add register to pool of free registers */
void RegisterPool_Add(int reg)
{
    reg -= 4; /* XXX: change this if register layout changes (e.g. to R0-R10) */
    register_pool[reg] = 1;
}

static int licomp_startpoint(const void* l1, const void* l2)
{
    LiveInterval* li1 = (LiveInterval*)l1;
    LiveInterval* li2 = (LiveInterval*)l2;
    if (li1->start > li2->start)
        return 1;
    if (li1->start < li2->start)
        return -1;
    return 0;
}

static int licomp_endpoint(const void* l1, const void* l2)
{
    LiveInterval* li1 = *(LiveInterval**)l1;
    LiveInterval* li2 = *(LiveInterval**)l2;
    if (li1->end > li2->end)
        return 1;
    if (li1->end < li2->end)
        return -1;
    return 0;
}

void LS_ClearLiveIntervals()
{
    nIntervals = 0;
}

/* preparatory for linear scan algorithm */
void LS_AddLiveInterval(int vreg, int start, int end)
{
    LiveIntervals[nIntervals].vreg = vreg;
    LiveIntervals[nIntervals].start = start;
    LiveIntervals[nIntervals].end = end;
    LiveIntervals[nIntervals].loc = 0;
    nIntervals++;
}

static void ExpireOldIntervals(int i)
{
    int j, removed_intervals=0;
    static LiveInterval dirty = {0, 0, 0, 0, ~0};

    //printf("activeset before: ");
    //for (j=0; j<nActive; j++) printf("%d ", ActiveSet[j]->end);
    //printf("\n");

    for (j=0; j<nActive; j++) { /* "foreach interval j in active, in order of increasing end point" */
        if (ActiveSet[j]->end >= LiveIntervals[i].start)
            break;
        
        RegisterPool_Add(ActiveSet[j]->reg);

        ActiveSet[j] = &dirty; /* mark as dirty for deletion through sort below */
        removed_intervals++;
    }

    /* remove entries from active set */
    qsort(&ActiveSet[0], nActive, sizeof(ActiveSet[0]), licomp_endpoint);
    nActive -= removed_intervals;

    //printf("activeset after: ");
    //for (j=0; j<nActive; j++) printf("%d ", ActiveSet[j]->end);
    //printf("\n");
}

extern int loc;
static inline int NewStackLocation()
{
    loc = (loc - 4) & -4;
    //return ++spill_counter;
    return loc;
}

static void SpillAtInterval(int i)
{
    LiveInterval* Spill = ActiveSet[nActive-1]; /* "last interval in active" */
    if (Spill->end > LiveIntervals[i].end) {
        LiveIntervals[i].reg = Spill->reg;
        Spill->loc = NewStackLocation();

        ActiveSet[nActive-1] = &LiveIntervals[i];
        qsort(&ActiveSet[0], nActive, sizeof(ActiveSet[0]), licomp_endpoint); /* sort by increasing end point */
    } else {
        LiveIntervals[i].loc = NewStackLocation();
    }
}

void LS_RegisterAllocation()
{
    int i;
    //printf("\nPerforming Linear Scan Register Allocation... (R=%d)\n", R);
    //printf("R=%d [R%d...R%d]\n", R, 4, 4+R-1);

    for (i=0; i<R; i++)
        register_pool[i] = 1; /* initially, all registers are free */

    spill_counter = 0; /* no registers are spilled */
    regs_used = 0; /* no regs are used */
    nActive = 0; /* empty active set */

    /* sort live intervals in order of increasing starting point */
    qsort(&LiveIntervals[0], nIntervals, sizeof(LiveIntervals[0]), licomp_startpoint);

    for (i=0; i<nIntervals; i++) { /* "foreach live interval, in order of increasing start point" */
        ExpireOldIntervals(i);
        if (nActive == R) {
            SpillAtInterval(i);
            //printf("Interval %d (%d,%d) got spilled\n", i, LiveIntervals[i].start, LiveIntervals[i].end);
        } else {
            LiveIntervals[i].reg = RegisterPool_Get();
            ActiveSet[nActive++] = &LiveIntervals[i];
            qsort(&ActiveSet[0], nActive, sizeof(ActiveSet[0]), licomp_endpoint); /* sort by increasing end point */
            //printf("Interval (%d,%d) got register R%d\n", LiveIntervals[i].start, LiveIntervals[i].end, LiveIntervals[i].reg);
        }
    }
    
    //printf("---\n");

    for (i=0; i<nIntervals; i++) {
#ifdef SHOW_ALLOCATION_RESULT
        printf("Interval %d (%d,%d), VReg%d --> ", i, LiveIntervals[i].start, LiveIntervals[i].end, LiveIntervals[i].vreg);
        if (LiveIntervals[i].loc) {
            printf("spilled at stack offset %d\n", LiveIntervals[i].loc);
        }
        else {
            printf("R%d\n", LiveIntervals[i].reg);
        }
#endif
        IR_ReplaceVReg(LiveIntervals[i].vreg, LiveIntervals[i].loc, LiveIntervals[i].reg); /* patch IR */
    }

    // also spill ignored vregs
    for (i=0; i<next_vreg_var; i++) {
        if (vreg_ignore_list[i] == 1)
            IR_ReplaceVReg(i, NewStackLocation(), 0);
    }
}
