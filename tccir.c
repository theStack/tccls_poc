#include "tcc.h"
#include "tccir.h"
#include "tccls.h"

#define REDUNDANT_MOVE_OPTIMIZATION

/* XXX: remove Vars[] array, as it is most probably not needed */
typedef struct {
	Sym *s;
	int c;
} LocalVar;
#define MAX_VARS 10000
#define MAX_TEMPS 10000
#define MAX_PARAMS 10000
LocalVar Vars[MAX_VARS];

typedef struct {
    int preg;
    int offset;
} VRegReplacement;
VRegReplacement RegMapping[MAX_VARS+MAX_TEMPS+MAX_PARAMS];

typedef struct {
    int start;
    int end; 
    int start_within_if;
} VRegInterval;
VRegInterval BaseIntervals[MAX_VARS+MAX_TEMPS+MAX_PARAMS];

#define MAX_QUADRUPLES 10000
Quadruple IR[MAX_QUADRUPLES];
uint32_t IR_addrs[MAX_QUADRUPLES];
const char* IRS[] = IR_OP_SYMBOLS;
uint8_t vreg_ignore_list[MAX_VARS];

int irpc;   		/* location of the next free quadruple */
int next_vreg_var;	/* next available virtual register (for local/global variables) */
int next_vreg_temp;	/* next available virtual register (for temporaries) */
int next_vreg_param; /* next available virtual register (for parameters) */
int num_params;      /* number of parameters for the current function */
int param_reg_list; /* registers used for par (set in IR_AddFuncParams(), used in IR_GenProlog()) */
int leaffunc;       /* set if function doesn't call other functions */
int bb_start;       /* set if next instruction is the first of a basic block */
int prevent_coalescing;
static int call_level; /* -1=not in any function call phase */
int num_params_per_level[50];
int within_if;


void IR_Clear()
{
  int i;
  //printf("sizeof(SValue)==%d, sizeof(Quadruple)==%d\n", sizeof(SValue), sizeof(Quadruple));
  irpc = 1;
  for (i=0; i<MAX_VARS; i++)
    vreg_ignore_list[i] = 0;

  next_vreg_var = 0;
  next_vreg_temp = MAX_VARS;
  next_vreg_param = MAX_VARS+MAX_TEMPS;
  memset(BaseIntervals, 0, sizeof(BaseIntervals));
  num_params = 0;
  leaffunc = 1;
  bb_start = 1;
  prevent_coalescing = 0;
  call_level = -1;
  loc = 0;
  within_if = 0;
}

int IR_AddLocalVar(Sym *s, int stack_offset) 
{
	Vars[next_vreg_var].s = s;
	Vars[next_vreg_var].c = stack_offset;	
	return IR_GetVregVar();
}

int IR_GetVregVar()  { return next_vreg_var++; }
int IR_GetVregTemp() { return next_vreg_temp++; }
int IR_GetVregParam() { return next_vreg_param++; }

int IR_GetVregType(int vreg)
{
    if (vreg < 0) return -1;
	if (vreg < MAX_VARS) return VREG_LOCALVAR;
    if (vreg < (MAX_VARS+MAX_TEMPS)) return VREG_TEMPORARY;
    if (vreg < (MAX_VARS+MAX_TEMPS+MAX_PARAMS)) return VREG_PARAMETER;
    return -1;
}
#define VREG_VALID(vreg) (vreg >= 0 && vreg < (MAX_VARS+MAX_TEMPS+MAX_PARAMS))

void IR_PreventAllocForVreg(int vreg)
{
    vreg_ignore_list[vreg] = 1;
}

void IR_PreventCoalescing()
{
    prevent_coalescing++;
}

void IR_AllowCoalescing()
{
    prevent_coalescing--;
}

void IR_StartBasicBlock(void)
{
    bb_start = 1;
}

void IR_AddFuncParams(CType *func_type)
{
    /* parts largely from arm-gen.c:gfunc_prolog() */
    Sym *sym, *sym2;
    int n, size, align, struct_ret = 0;

    sym = func_type->ref;
    n = 0;
    /* XXX: handle variadic functions, handle struct returns */

    for (sym2=sym->next; sym2; sym2=sym2->next) { /* calculate number of argument registers used */
        size = type_size(&sym2->type, &align);
        if (n < 4)
            n += (size + 3) / 4;
    }
    if (n > 4) /* four arguments on register maximum (r0-r3) */
        n = 4;
#ifdef TCC_ARM_EABI
    n=(n+1)&-2; /* XXX: why? 8 byte alignment at public interface? */
#endif
    param_reg_list = (1<<n)-1;

  {
    int addr, pn = struct_ret, sn = 0; /* pn=core, sn=stack */

    while ((sym = sym->next)) {
      CType *type;
      int flags;
      type = &sym->type;
      size = type_size(type, &align);
      size = (size + 3) >> 2;
      align = (align + 3) & ~3;
      if (pn < 4) {
#ifdef TCC_ARM_EABI
        pn = (pn + (align-1)/4) & -(align/4);
#endif
        addr = pn * 4;
        pn += size;
        if (!sn && pn > 4)
          sn = (pn - 4);
      } else {
#ifdef TCC_ARM_EABI
        sn = (sn + (align-1)/4) & -(align/4);
#endif
        addr = (n + sn) * 4;
        sn += size;
      }
      //sym_push(sym->v & ~SYM_FIELD, type, VT_LOCAL | lvalue_type(type->t), addr+12);
      flags = VT_LOCAL | lvalue_type(type->t);
      if (pn <= 4)
        flags |= VT_PARAM; /* set param flag for first four arguments */
      sym_push(sym->v & ~SYM_FIELD, type, flags, addr+40); /* XXX: adapt this value! */
      num_params++;
    }
  }
}

int IR_PutOp(int op, SValue *src1, SValue *src2, SValue *dest)
{
  if (irpc == MAX_QUADRUPLES)
    tcc_error("compiler error! function \"%s\" too large, no space left for intermediate representation", funcname);

  //printf("IR_PutOp called with irpc=%d, op==%d\n", irpc, op);

  /* special case: subtraction with immediate value as first operand, substitute SUB with RSUB */
  if ((op == IR_OP_SUB) && ((src1->r & (VT_VALMASK | VT_LVAL | VT_SYM)) == VT_CONST)) {
    SValue *tmp = src1; /* swap operands */
    src1 = src2;
    src2 = tmp;
    op = IR_OP_RSUB;
  }

  IR[irpc].op = op;
  /* void return needs no source operands */
  if ((op != IR_OP_RETURNVOID) && (op != IR_OP_JMP) && (op != IR_OP_FUNCPARAMVOID)) {
    IR[irpc].src1 = *src1;
    if (VREG_VALID(src1->vreg)) { /* extend base interval end (last usage) */
        BaseIntervals[src1->vreg].end = irpc;
        //printf("set v%d.end to %d\n", src1->vreg, irpc);
    }
  } else
    IR[irpc].src1.vreg = -1; /* for simplifying data-flow-analysis */

   /* assignment/store/return only needs one source operand (maximum) */
  if ((op != IR_OP_ASS) && (op != IR_OP_STORE) && (op != IR_OP_RETURNVOID) && (op != IR_OP_RETURNVAL) &&
      (op != IR_OP_FUNCCALLVOID) && (op != IR_OP_FUNCCALLVAL) && (op != IR_OP_SETIF) && (op != IR_OP_BRANCHIF) && (op != IR_OP_JMP) && (op != IR_OP_TSTZ) && (op != IR_OP_FUNCPARAMVOID)) {
  	IR[irpc].src2 = *src2;
    if (VREG_VALID(src2->vreg)) { /* extend base interval end (last usage) */
        BaseIntervals[src2->vreg].end = irpc;
        //printf("set v%d.end to %d\n", src2->vreg, irpc);
    }
  } else
    IR[irpc].src2.vreg = -1; /* for simplifying data-flow-analysis */

  if ((op != IR_OP_RETURNVOID) && (op != IR_OP_RETURNVAL) && (op != IR_OP_FUNCPARAM) && (op != IR_OP_FUNCCALLVOID) && (op != IR_OP_CMP) && (op != IR_OP_TSTZ) && (op != IR_OP_FUNCPARAMVOID)) {
    IR[irpc].dest = *dest;
    if (VREG_VALID(dest->vreg) && BaseIntervals[dest->vreg].start == 0) { /* set base interval start (first def) */
      BaseIntervals[dest->vreg].start = irpc;
      /* set flag if first definition takes place within a conditional */
      if (within_if && IR_GetVregType(dest->vreg) == VREG_LOCALVAR) {
          BaseIntervals[dest->vreg].start_within_if = 1;
          //printf("vreg%d definition within if!\n", dest->vreg);
      }
      //printf("set v%d.start to %d\n", dest->vreg, irpc);
    }
    if (VREG_VALID(dest->vreg)) /* extend base interval end (last usage) */
      BaseIntervals[dest->vreg].end = irpc;
      //printf("set v%d.end to %d\n", dest->vreg, irpc);
  } else
    IR[irpc].dest.vreg = -1; /* for simplifying data-flow-analysis */

  /* unset leaf function flag as soon as a function call occurs */
  if ((op == IR_OP_FUNCCALLVOID) || (op == IR_OP_FUNCCALLVAL))
    leaffunc = 0;

  /* no physical register assigned yet */
  IR[irpc].src1.preg = -1;
  IR[irpc].src2.preg = -1;
  IR[irpc].dest.preg = -1;

#ifdef REDUNDANT_MOVE_OPTIMIZATION
  /* optimization: eliminate redundant moves (XXX: may not work over basic block boundaries) */
  if (bb_start)
    bb_start = 0;
  else if ((!prevent_coalescing) && (irpc > 1) && (op == IR_OP_ASS) && (IR_GetVregType(src1->vreg) == VREG_TEMPORARY) &&
    ((src1->r & VT_LVAL) == 0) && (src1->vreg == IR[irpc-1].dest.vreg) && (IR[irpc-1].op != IR_OP_FUNCCALLVAL)) {
    //printf("the following move (dest=vreg%d) could be eliminated!\n", src1->vreg);
    IR[irpc-1].dest = IR[irpc].dest;
    //printf("[PATCHED] "); print_quadruple(&IR[irpc-1], irpc-1);
    BaseIntervals[src1->vreg].start = 0; /* discard base interval of eliminated vreg */
    BaseIntervals[src1->vreg].end = 0;
    if (VREG_VALID(IR[irpc].dest.vreg) && BaseIntervals[IR[irpc].dest.vreg].start == irpc)
        BaseIntervals[IR[irpc].dest.vreg].start = irpc-1;
    return irpc;
  }
#endif
  //print_quadruple(&IR[irpc], irpc);
  return irpc++;
}

int IR_GetPC()
{
  return irpc;
}

void IR_DropReturnValue()
{
  Quadruple *last_instr;
  if (irpc == 1)
    return;
  last_instr = &(IR[irpc-1]);

  /* if last IR instruction was function call with return value, substitute with void function call */
  if (last_instr->op == IR_OP_FUNCCALLVAL) {
    last_instr->op = IR_OP_FUNCCALLVOID;
    BaseIntervals[last_instr->dest.vreg].start = 0; /* discard base interval of dropped vreg */
    BaseIntervals[last_instr->dest.vreg].end = 0;
    last_instr->dest.vreg = -1;
    last_instr->src1.vreg = -1;
  }
}

void IR_BackpatchFirst(int t, int target_addr) /* similar to patch part in gtst() */
{
  int lp;
  do {
    lp = t;
    t = IR[t].dest.c.i;
  } while (t); /* traverse list to the end */

  IR[lp].dest.c.i = target_addr;
}

void IR_Backpatch(int t, int target_addr) /* similar to gsym_addr() */
{
  SValue *cur;
  //printf("Backpatch called, t==%d, target_addr=%d\n", t, target_addr);

  while (t) {
    cur = &(IR[t].dest);

    t = cur->c.i; /* select next element in linked list */
    cur->c.i = target_addr; /* patch entry */
  }
}

void IR_BackpatchToHere(int t) /* similar to gsym() */
{
  IR_Backpatch(t, irpc);
}


static int FindLiveInterval(int vreg, unsigned int *start, unsigned int *end, int check_for_backwards_jumps)
{
    int i, retval;
    Quadruple* quad;

    retval = 0;
    
    *start = BaseIntervals[vreg].start;
    *end = BaseIntervals[vreg].end;
    if (*start > 0 && *end > 0)
        retval = 1;
    
    if (!check_for_backwards_jumps)
        return retval;
    
	/* extend interval end if there is a later backwards jump with a lower target address */
    i = *end;
    quad = &IR[i];
    for (;i<irpc;i++) {
        if ((quad->op == IR_OP_JMP || quad->op == IR_OP_BRANCHIF) && 
            (quad->dest.c.i <= *end) && (quad->dest.c.i > *start)) {
            *end = i;
        }
		quad++;
    }
 
    /* special treatment for first definitions within conditional: extend interval to outermost loop */
    if (BaseIntervals[vreg].start_within_if) {
        quad = &IR[irpc-1];
        for (i=irpc-1; i>*start; i--) {
            if ((quad->op == IR_OP_JMP || quad->op == IR_OP_BRANCHIF) &&
                (quad->dest.c.i < *start)) {
                *start = quad->dest.c.i;
                if (i > *end) *end = i;
                break;
            }
            quad--;
        }
        for (i=*start; i<*end; i++) {
            quad = &IR[i];
            if ((quad->op == IR_OP_JMP || quad->op == IR_OP_BRANCHIF) &&
                (quad->dest.c.i < *start)) {
                *start = quad->dest.c.i;
                break;
            }
        }
    }

    return retval;
}

//#define SHOW_LIVENESS_RESULT
void IR_LivenessAnalysis()
{
    unsigned int vreg, start, end;

    LS_ClearLiveIntervals();

    /* local variable vregs */
    for (vreg=0; vreg<next_vreg_var; vreg++) {
        if (vreg_ignore_list[vreg] == 1) {
#ifdef SHOW_LIVENESS_RESULT
            printf("VReg%d --> Ignored for register allocation!\n", vreg);
#endif
            continue;
        }
        start = 0, end = ~0; /* for debugging purposes (maybe leave it like that) */
        if (FindLiveInterval(vreg, &start, &end, 1)) {
#ifdef SHOW_LIVENESS_RESULT
            printf("VReg%d --> Interval (%d,%d)\n", vreg, start, end);
#endif
            LS_AddLiveInterval(vreg, start, end);
        }
    }

    /* temporary vregs */
    for (vreg=MAX_VARS; vreg<next_vreg_temp; vreg++) {
        start = 0, end = ~0; /* for debugging purposes (maybe leave it like that) */
        if (FindLiveInterval(vreg, &start, &end, 0)) {
#ifdef SHOW_LIVENESS_RESULT
            printf("VReg%d --> Interval (%d,%d)\n", vreg, start, end);
#endif
            LS_AddLiveInterval(vreg, start, end);
        }
    }
}

#define PREG_SPILLED 0x100000
void IR_ReplaceVReg(int vreg, int offset, int preg)
{
    RegMapping[vreg].preg = offset ? PREG_SPILLED : preg; /* if offset != 0, register has been marked as spilled */
    RegMapping[vreg].offset = offset;
}

void IR_Show()
{
    int i;
    //printf("--- Memory occupied for IR: %d bytes statically, max. %d entries (one entry: %d bytes) ---\n",
    //	sizeof(IR), MAX_QUADRUPLES, sizeof(IR[0]));
    //XXX: output summary of occupied memory, also including IR_addrs[] array (and others)!

    for (i=1; i<irpc; i++) {
        print_quadruple(&IR[i], i);
    }
}


static int OperandInMemory(SValue *sv)
{
    int svt = sv->r & VT_VALMASK;

    /* XXX: take into account VT_LVAL? */

    if (sv->preg == -1) { /* not register-allocated */
        if (svt == VT_LOCAL)
            return 1;
        else if (svt == VT_CONST) {
            return sv->r & VT_SYM; /* VT_SYM set: global variable, VT_SYM not set: immediate value */
        } else
            tcc_error("SValue not register-allocated (neither stack value, nor global/static variable, nor constant), sorry...");
    } else if (sv->preg & PREG_SPILLED) { /* register-allocated, but spilled */
        return 1;
    } 

	/* everything fine, register already known */
	return 0;
}

extern int ind;
extern Section *cur_text_section;
static void GenConstIntoReg(int r, uint32_t c);
/* load spilled register into spill_reg */
static int LoadIntoReg(SValue *sv, int spill_reg)
{
    int vt, opc;
    int sign, immval;
    vt = sv->type.t;
    //printf("\t sv->type.t == 0x%x, sv->r == 0x%x\n", vt, sv->r);

    immval = sv->c.i;
    if (immval >= 0) {
        sign = 0;
    } else {
        sign = 1;
        immval = -immval;
    }

    /* from tccgen.c:gv(), this could probably be done more elegantly */
    ////if (sv->preg != PREG_SPILLED) {
    ////if (!((sv->preg == PREG_SPILLED) && (IR_GetVregType(sv->vreg) == VREG_TEMPORARY))) {
    if (sv->r & VT_LVAL_BYTE)
        vt = VT_BYTE;
    else if (sv->r & VT_LVAL_SHORT)
        vt = VT_SHORT;
    if (sv->r & VT_LVAL_UNSIGNED)
        vt |= VT_UNSIGNED;
    ////}

    /***************************** global variables *************************/
    if (((sv->r & VT_VALMASK) == VT_CONST) && (sv->r & VT_SYM)) {
        o(0xE59F0000 | (spill_reg << 12)); /* mov rX, [pc] */
        o(0xEA000000); /* skip next instruction (which is in fact, data) */
        greloc(cur_text_section, sv->sym, ind, R_ARM_ABS32); /*#### linker data ####*/
        o(sv->c.ul);

        return spill_reg;
    }

    /************************* local/spilled variables **********************/
    ////if (!(sv->r & VT_LVAL) && !(sv->preg == PREG_SPILLED)) {
    ////if (!((sv->preg == PREG_SPILLED) && (IR_GetVregType(sv->vreg) == VREG_TEMPORARY)) && !(sv->r & VT_LVAL)) {
    if (!(sv->r & VT_LVAL)) {
        opc = 0xE24B0000; /* sub */
        if (!sign)
            opc = 0xE28B0000; /* add */
        if (immval > 255) {
            opc &= ~0x02000000; /* remove I bit */
            GenConstIntoReg(spill_reg, immval);
            o(opc | (spill_reg << 12) | spill_reg);
        } else 
            o(opc | (spill_reg << 12) | immval);
        return spill_reg;
    }

    /*...*/
    if ((vt & VT_BTYPE) == VT_SHORT ||  /* short signed/unsigned */
        (vt & (VT_BTYPE|VT_UNSIGNED)) == VT_BYTE) { /* signed byte */
        opc = 0xE1500090;
        if ((vt & VT_BTYPE) == VT_SHORT)
            opc |= 0x20;
        if ((vt & VT_UNSIGNED) == 0)
            opc |= 0x40;
        if ((vt & VT_BTYPE) == VT_BYTE)
            opc |= 0x400000;
        if (!sign)
            opc |= 0x800000;
        if (immval > 255) {
            opc &= ~0x400000; // immediate offset->register offset
            GenConstIntoReg(spill_reg, immval);
            o(opc | (spill_reg << 12) | (0xB << 16) | spill_reg);
            //tcc_error("LoadIntoReg(): memory offsets only possible in range (0,255) for halfwords or signed bytes");
        } else
          o(opc | (spill_reg << 12) | (0xB << 16) | ((immval & 0xf0) << 4) | (immval & 0x0f));
    } else { /* word or unsigned byte */
        opc = 0xE5100000;
        /* XXX: take offset sign into account (now we assume always negative) */
        if ((vt & VT_BTYPE) == VT_BYTE)
            opc |= 0x400000;
        if (!sign)
            opc |= 0x800000;
        if (immval > 4095) {
            opc |= 1<<25; /* set I bit (has opposite meaning than for data-processing instrs. here) */
            GenConstIntoReg(spill_reg, immval);
            o(opc | (spill_reg << 12) | (0xB << 16) | spill_reg);
        } else
            o(opc | (spill_reg << 12) | (0xB << 16) | immval);
    } 

    return spill_reg;
}

/* determine temporary register for storing */
static void StoreFromReg(SValue *sv, int spill_reg, int offset_reg)
{
    int vt, opc;
    int sign, immval;

    /* XXX: take VT_LVAL into account? */

    immval = sv->c.i;
    if (immval >= 0) {
        sign = 0;
    } else {
        sign = 1;
        immval = -immval;
    }

    vt = sv->type.t;
    //printf("\t sv->type.t == 0x%x, sv->r == 0x%x\n", vt, sv->r);

    /* from tccgen.c:gv(), this could probably be done more elegantly */
    if (sv->r & VT_LVAL_BYTE)
        vt = VT_BYTE;
    else if (sv->r & VT_LVAL_SHORT)
        vt = VT_SHORT;
    if (sv->r & VT_LVAL_UNSIGNED)
        vt |= VT_UNSIGNED;

    /*...*/
    if ((vt & VT_BTYPE) == VT_SHORT) { /* short signed/unsigned */
        opc = 0xE14000B0;
        if (!sign)
            opc |= 0x800000;
        if (immval > 255) {
            opc &= ~0x400000; // immediate offset->register offset
            GenConstIntoReg(offset_reg, immval);
            //tcc_error("StoreFromReg(): memory offsets only possible in range (0,255) for halfwords or signed bytes");
            o(opc | (spill_reg << 12) | (0xB << 16) | offset_reg);
        } else
            o(opc | (spill_reg << 12) | (0xB << 16) | ((immval & 0xf0) << 4) | (immval & 0x0f));
    } else { /* byte or word signed/unsigned */
        opc = 0xE5000000;
        if ((vt & VT_BTYPE) == VT_BYTE)
            opc |= 0x400000;
        if (!sign)
            opc |= 0x800000;
        if (immval > 4095) {
            opc |= 1<<25; /* set I bit (has opposite meaning than for data-processing instrs. here) */
            GenConstIntoReg(offset_reg, immval);
            o(opc | (spill_reg << 12) | (0xB << 16) | offset_reg);
        } else
            o(opc | (spill_reg << 12) | (0xB << 16) | immval);
    }
}

/* evaluate constant into register if necessary */
static void GenConstIntoReg(int r, uint32_t c)
{
    // XXX: if costs maximum 2 instructions, use the following strategy
#if 0
    int i, immval, rotval, count=0;
    uint32_t opc;

    for (i=0; i<32; i+=8) {
        immval = (c & (0xff << i)) >> i;
        if (immval) {
            rotval = i ? (16 - i/2) : 0;
            if (count++ == 0) {
              opc = 0xE3A00000 | (r<<12);
              printf("\tmov r%d, #0x%08x\n", r, immval << i);
            }
            else {
              opc = 0xE3800000 | (r<<16) | (r<<12);
              printf("\torr r%d, r%d, #0x%08x\n", r, r, immval << i);
            }
            opc |= (rotval << 8) | immval;
            o(opc);
        }
    }
	return;
#endif
    if (c <= 255) { 
        o(0xE3A00000 | (r << 12) | c);
        return;
    }

    o(0xE59F0000 | (r << 12)); /* mov rX, [pc] */
    o(0xEA000000); /* skip next instruction (which is in fact, data) */
    o(c);
}

static int ConstIntoReg(SValue* sv, int target_reg)
{
    if ((sv->r & VT_VALMASK) == VT_CONST) {
        GenConstIntoReg(target_reg, sv->c.i);
        return target_reg;
    }
    return sv->preg;
}

void o(uint32_t i);
static void GenDataProcessingOp(int op, int dest_reg, int src1_reg, int src2_reg, int second_const, int cval)
{
    //static const char* DataProcessingMnemos[] = /* for debugging purposes */
    //  { "add", "sub", "and", "orr", "xor", "lsl", "lsr", "asr", "mov", "rsb", "cmp", "teq" };
    //const char* ops = DataProcessingMnemos[op];
    static const int DataProcessingOpMap[] = 
      { 4, 2, 0, 12, 1, 13,13,13,13, 3, 10, 9 }; /* add, sub, and, or, xor, [shl, shr, sar], mov, rsb, cmp, teq */
    
    uint32_t opc = 0xE0000000; /* unconditional execution */
    int dpopc = DataProcessingOpMap[op]; /* data-processing opcode */

    if ((op == IR_OP_CMP) || (op == IR_OP_TSTZ)) { /* overrule for comparison ops */
      opc |= (1<<20); /* set condition codes */
      dest_reg = 0;
    }

    /* XXX: handle global variable access (VT_SYM?) */
    if (op == IR_OP_ASS) { /* source operand for pure mov must be in op2 field, op1 must be zero */
      if (src1_reg == dest_reg) /* omit instruction mov rX, rX (is like nop) */
          return;
      src2_reg = src1_reg;
      src1_reg = 0;
    }

    opc |= (dpopc << 21); /* set data-processing opcode [I=0, S=0] */
    opc |= (dest_reg << 12); /* set destination register */

    if (op == IR_OP_SHL || op == IR_OP_SHR || op == IR_OP_SAR) {
		/* special cases: shifts */
        opc |= src1_reg; /* set source register */
        if (op == IR_OP_SHR)
          opc |= 1<<5;
        if (op == IR_OP_SAR)
          opc |= 1<<6;	
        if (!second_const)
          opc |= 1<<4 | (src2_reg << 8);
        else
          opc |= (cval & 0x1f) << 7;
    } else {
      opc |= (src1_reg << 16); /* set first source register (0 for mov) */
      if (!second_const)
        opc |= src2_reg; /* set second source register */
      else {
        if (cval < 0) { /* see arm-gen.c:stuff_const() for more advanced constant handling */
            if (op == IR_OP_ADD || op == IR_OP_SUB) {
              opc ^= 0x00C00000;
              cval = -cval;
            }
            else if (op == IR_OP_ASS) {
              opc ^= 0x00400000;
              op = -1; // invalidate op (prevents optimization assuming "mov" instr., see below)
              cval = ~cval;
            }
        }
        /* if constant is too large (more than 8-bits wide), we have to evaluate it into register first */
        if (cval > 255 || cval < 0) {
          GenConstIntoReg(14, cval);
          if ((op == IR_OP_ASS) && (dest_reg == 14)) /* no need to generate mov r14, r14 (nop) */
              return;
          opc |= 14;
        } else {
          opc |= 1<<25; /* set I bit */
          opc |= cval & 0xff;
        }
      }
   }
   o(opc);
}

static uint32_t mapcc(int cc) /* from arm-gen.c */
{
  switch(cc)
  {
    case TOK_ULT: return 0x30000000; /* CC/LO */
    case TOK_UGE: return 0x20000000; /* CS/HS */
    case TOK_EQ:  return 0x00000000; /* EQ */
    case TOK_NE:  return 0x10000000; /* NE */
    case TOK_ULE: return 0x90000000; /* LS */
    case TOK_UGT: return 0x80000000; /* HI */
    case TOK_LT:  return 0xB0000000; /* LT */
    case TOK_GE:  return 0xA0000000; /* GE */
    case TOK_LE:  return 0xD0000000; /* LE */
    case TOK_GT:  return 0xC0000000; /* GT */
  }
  tcc_error("unexpected condition code");
  return 0xE0000000; /* AL */
}

static int negcc(int cc) /* from arm-gen.c */
{
  switch(cc)
  {
    case TOK_ULT: return TOK_UGE;
    case TOK_UGE: return TOK_ULT;
    case TOK_EQ:  return TOK_NE;
    case TOK_NE:  return TOK_EQ;
    case TOK_ULE: return TOK_UGT;
    case TOK_UGT: return TOK_ULE;
    case TOK_LT:  return TOK_GE;
    case TOK_GE:  return TOK_LT;
    case TOK_LE:  return TOK_GT;
    case TOK_GT:  return TOK_LE;
  }
  tcc_error("unexpected condition code");
  return TOK_NE;
}

static void FuncCallParam(int param_num, int src_reg, int isconst, int cval)
{

#if 0
        /* save argument registers (from currently generated function!) on stack */
        if (param_reg_list)
            o(0xE92D0000 | param_reg_list); /* push arguments */
#endif
    
    if (param_num > 4) {
        //tcc_error("function calls: more than 4 arguments (that is, arguments on the stack) are not supported yet!\n");
        if (isconst) {
            GenConstIntoReg(src_reg=14, cval);
        }
	    o(0xE52D0004|(src_reg<<12)); /* str r,[sp,#-4]! */
        return;
    }

    if (param_num == 1) { /* new function call phase starts */
        call_level++;
        if (call_level >= 1) { /* if we have nested function calls, we have to save arg regs (r0-r3) in-between */
            int args_to_save = num_params_per_level[call_level-1];
            if (args_to_save > 4)
                args_to_save = 4;
            o(0xE92D0000 | ((1<<args_to_save)-1)); /*P=1, U=0, W=1, L=0*/
        }
    } else if (param_num == 0) { /* special case: for void function calls with return value, save r0 in-between */
        call_level++;
        if (call_level >= 1)
            o(0xE92D0001);
        num_params_per_level[call_level] = 1;
        return;
    }

    GenDataProcessingOp(IR_OP_ASS, param_num-1, src_reg, -1, isconst, cval);
    num_params_per_level[call_level] = param_num;
}

static void FuncCallAfter()
{
    if (call_level >= 1) {
        int args_to_restore = num_params_per_level[call_level-1];
        o(0xE8BD0000 | ((1<<args_to_restore)-1)); /*P=0, U=1, W=1, L=1*/
    }
        
    call_level--;
}

extern int regs_used; // tccls.c

static void GenProlog()
{
  int save_reg_list;
  int ignore_fp_sp = (leaffunc && num_params <= 4 && loc == 0); // criterion for optimizing fp and sp away

  save_reg_list = ((1 << regs_used)-1) << 4; /* save regs r4-r10 if needed */
  save_reg_list |= 0x5800; /* save r11=fp (frame pointer), r12=ip (stack pointer), r14=lr (return addres) */
  if (ignore_fp_sp) {
     save_reg_list &= ~((1 << 11) | (1 << 12));
  } else
    o(0xE1A0C00D); /* mov ip, sp -- (since stack pointer can't be used directly in ldm/stm instructions) */
  if (num_params > 0 && (!leaffunc || num_params > 4)) {
      o(0xE92D0000 | param_reg_list); /* push arguments */
      o(0xE24DD000 | ((7-regs_used)*4)); /* adapt stack pointer such that argument offsets are ok */ /* XXX: optimize this away..... */
  }
  o(0xE92D0000 | save_reg_list); /* push desired registers on stack (context save) */
  if (!ignore_fp_sp)
    o(0xE1A0B00D); /* mov fp, sp */
  if (loc) {
      int diff = (-loc + 3) & -4; /* align to multiple of 4 */
      if (!leaffunc)
          diff = ((diff + 11) & -8) - 4; /* align to multiple of 8 */
      if (diff > 255) {
          GenConstIntoReg(12, diff);
          o(0xE04DD00C); /* substract with ip */
      } else
          o(0xE24DD000 | diff); /* stack adjustment; */
  }
}

static void GenEpilog()
{
  int restore_reg_list;
  int ignore_fp_sp = (leaffunc && num_params <= 4 && loc == 0); // criterion for optimizing fp and sp away

  restore_reg_list = ((1 << regs_used)-1) << 4; /* restore regs r4-r10 if needed */
  restore_reg_list |= 0xA800; /* restore r11=fp (frame pointer), r13=sp (stack pointer), r15=pc (program counter) */
  if (ignore_fp_sp) {
    restore_reg_list &= ~((1 << 11) | (1 << 13));
  }

  if (!ignore_fp_sp)
    o(0xE89B0000 | restore_reg_list); /* restore desired registers from stack (context restore) */
  else
    o(0xE8BD0000 | restore_reg_list); /* if fp and sp are ignored, directly pop! */
}

void IR_RegisterAllocationParams()
{
  if (leaffunc) {
    int vreg,argno;
    for (vreg=MAX_VARS+MAX_TEMPS,argno=0; vreg<next_vreg_param; vreg++,argno++) {
      if (argno <= 3)
        IR_ReplaceVReg(vreg, 0, argno); /* patch parameter vregs */
    }
  }
}

static void LoadRegisterIndirect(SValue *sv, int dest_reg, int src_reg)
{
	int opc, vt;

    vt = sv->type.t; /* from gv() */
    if (sv->r & VT_LVAL_BYTE)
        vt = VT_BYTE;
    else if (sv->r & VT_LVAL_SHORT)
        vt = VT_SHORT;
    if (sv->r & VT_LVAL_UNSIGNED)
        vt |= VT_UNSIGNED;

	if ((vt & VT_BTYPE) == VT_SHORT ||  /* short signed/unsigned */
		(vt & (VT_BTYPE|VT_UNSIGNED)) == VT_BYTE) { /* signed byte */
		opc = 0xE1D00090;
		if ((vt & VT_BTYPE) == VT_SHORT)
			opc |= 0x20;
		if ((vt & VT_UNSIGNED) == 0)
			opc |= 0x40;
		if ((vt & VT_BTYPE) == VT_BYTE)
			opc |= 0x400000;
		o(opc | (dest_reg << 12) | (src_reg << 16));
	} else { /* word or unsigned byte */
		opc = 0xE5900000;
		if ((vt & VT_BTYPE) == VT_BYTE)
			opc |= 0x400000;
		o(opc | (dest_reg << 12) | (src_reg << 16));
	} 
}

void IR_GenCode()
{
  int i, op, opc;
  SValue *dest, *src1, *src2;
  int dest_reg, src1_reg, src2_reg;

  GenProlog();

  for (i=1; i<irpc; i++) {
    //printf("[%04d, real=%04x]\n", i, ind);
    IR_addrs[i] = ind;
    op = IR[i].op;
    dest = &(IR[i].dest);
    src1 = &(IR[i].src1);
    src2 = &(IR[i].src2);

    /* patch operands */
#define GRAB_VREG_MAPPING(op) { op->preg = RegMapping[op->vreg].preg; op->c.i = RegMapping[op->vreg].offset; }
    if (VREG_VALID(dest->vreg)) GRAB_VREG_MAPPING(dest)
    if (VREG_VALID(src1->vreg)) GRAB_VREG_MAPPING(src1)
    if (VREG_VALID(src2->vreg)) GRAB_VREG_MAPPING(src2)

    dest_reg = -1; /* maybe not needed... */
    src1_reg = -1;
    src2_reg = -1;

    /* determine destination register (if needed) */
    if ((op != IR_OP_RETURNVOID) && (op != IR_OP_RETURNVAL) && (op != IR_OP_FUNCPARAM) && (op != IR_OP_FUNCCALLVOID) && (op != IR_OP_STORE) && (op != IR_OP_CMP) && (op != IR_OP_BRANCHIF) && (op != IR_OP_JMP) && (op != IR_OP_TSTZ) && (op != IR_OP_FUNCPARAMVOID)) {
    	dest_reg = OperandInMemory(dest) ? 14 : dest->preg;
    } else if (op == IR_OP_STORE) {
        dest->r &= ~VT_LVAL; /* we only want to load the address */
        dest_reg = OperandInMemory(dest) ? LoadIntoReg(dest, 14) : dest->preg;
    }
    /* determine first source register (if needed) */
    if ((op != IR_OP_RETURNVOID) && (op != IR_OP_FUNCCALLVOID) && (op != IR_OP_FUNCCALLVAL) && (op != IR_OP_SETIF) && (op != IR_OP_BRANCHIF) && (op != IR_OP_JMP) && (op != IR_OP_FUNCPARAMVOID)) {
        if (op == IR_OP_ASS && OperandInMemory(src1) && dest_reg != 14 && 
            !((src1->r & VT_LVAL) && ((src1->r & VT_VALMASK) != VT_LOCAL))) {
            // direct load, to save move from r14 to target register
            LoadIntoReg(src1, dest_reg);
            continue;
        }
        src1_reg = OperandInMemory(src1) ? LoadIntoReg(src1, 12) : src1->preg;
        if (((src1->r & VT_LVAL) && ((src1->r & VT_VALMASK) != VT_LOCAL)) && (op != IR_OP_ASS)) {
            LoadRegisterIndirect(src1, 12, src1_reg); /* XXX: probably need to move to r12 first */
            src1_reg = 12;
        }
    }
    /* determine second source register (if needed) */
    if ((op != IR_OP_ASS) && (op != IR_OP_STORE) && (op != IR_OP_RETURNVOID) && (op != IR_OP_RETURNVAL) &&
        (op != IR_OP_FUNCPARAM) && (op != IR_OP_FUNCCALLVOID) && (op != IR_OP_FUNCCALLVAL) && (op != IR_OP_SETIF) && (op != IR_OP_BRANCHIF) && (op != IR_OP_JMP) && (op != IR_OP_TSTZ) && (op != IR_OP_FUNCPARAMVOID)) {
        src2_reg = OperandInMemory(src2) ? LoadIntoReg(src2, 14) : src2->preg;
        if (((src2->r & VT_LVAL) && ((src2->r & VT_VALMASK) != VT_LOCAL)) && (op != IR_OP_ASS)) {
            LoadRegisterIndirect(src2, 14, src2_reg); /* XXX: probably need to move to r12 first */
            src2_reg = 14;
        }
    }

    /* special treatment for multiplication, operands always have to be registers */
    if (op == IR_OP_MUL) {
        if (src1_reg != 12) src1_reg = ConstIntoReg(src1, 12);
        if (src2_reg != 14) src2_reg = ConstIntoReg(src2, 14);
    } else if (op == IR_OP_STORE) {
        if (src1_reg != 12) src1_reg = ConstIntoReg(src1, 12);
    } else if ((op == IR_OP_CMP) || (op == IR_OP_SHL) || (op == IR_OP_SHR) || (op == IR_OP_SAR)) { /* if first value is const, evaluate it into reg... */
        if (src1_reg != 12) src1_reg = ConstIntoReg(src1, 12); /* XXX: maybe this is also needed for other operators, if they are not commutative? */
    }

    switch (op) {
    case IR_OP_ASS:
      /* differentiate between register move and load */
      /* for now we assume only register move... */
      if ((src1->r & VT_LVAL) && ((src1->r & VT_VALMASK) != VT_LOCAL)) {
            LoadRegisterIndirect(src1, dest_reg, src1_reg);
      } else
          GenDataProcessingOp(op, dest_reg, src1_reg, src2_reg, (src1->r & (VT_VALMASK|VT_SYM)) == VT_CONST, src1->c.i);
      break;
    case IR_OP_ADD:
    case IR_OP_SUB:
    case IR_OP_AND:
    case IR_OP_XOR:
    case IR_OP_OR:
    case IR_OP_SHL:
    case IR_OP_SHR:
    case IR_OP_SAR:
    case IR_OP_RSUB:
    case IR_OP_CMP:
      //if ((src2->r & VT_LVAL) && ((src2->r & VT_VALMASK) != VT_LOCAL))
      //    tcc_error("indirection for operand 2, operation %s, not impl. yet!", IRS[op]);
      GenDataProcessingOp(op, dest_reg, src1_reg, src2_reg, (src2->r & (VT_VALMASK|VT_SYM)) == VT_CONST, src2->c.i);
      break;
    case IR_OP_TSTZ:
      GenDataProcessingOp(op, dest_reg, src1_reg, src2_reg, 1, 0); /* teq rX, #0 */
      break;
    case IR_OP_SETIF:
      o(0x03A00001 | mapcc(src1->c.i) | (dest_reg<<12));         /* if true, set #1 */
      o(0x03A00000 | mapcc(negcc(src1->c.i)) | (dest_reg<<12));  /* if not true, set #0 */
      break;
    case IR_OP_BRANCHIF:
      o(mapcc(src1->c.i) | 0x0a000000); /* branch offset is patched later */
      break;
    case IR_OP_JMP:
      o(0xea000000); /* branch offset is patched later */
      break;
    case IR_OP_MUL:
      /* "The destination register Rd must not be the same as the operand register Rm." */
      if (dest_reg == src1_reg) {
        int temp = src1_reg;
        src1_reg = src2_reg;
        src2_reg = temp;
      }
      if (dest_reg == src1_reg) /* all three operands are in the same reg, problem... */
        tcc_error("mul instruction with Rd == Rm, not allowed!");
      o(0xE0000090 | (dest_reg << 16) | (src2_reg << 8) | src1_reg);
      break;
#if 0
    case IR_OP_SDIV:
      printf("TODO: create function call for signed division here!\n");
      break;
    case IR_OP_UDIV:
      printf("TODO: create function call for unsigned division here!\n");
      break;
    case IR_OP_SMOD:
      printf("TODO: create function call for signed modulo here!\n");
      break;
    case IR_OP_UMOD:
      printf("TODO: create function call for unsigned modulo here!\n");
      break;
#endif
    /* TODO: conditionals missing */
    case IR_OP_STORE: /* special instruction...*/
      /* XXX: maybe don't distinguish between OP_ASS and OP_STORE??? */
      /*...*/
      if (dest->r & VT_LVAL_SHORT) { /* short signed/unsigned */
        opc = 0xE1C000B0; // [UP bit set, though it doesn't make a difference]
        o(opc | (src1_reg << 12) | (dest_reg << 16));
      } else { /* byte or word signed/unsigned */
        opc = 0xE5800000; // [UP bit set, though it doesn't make a difference]
        if (dest->r & VT_LVAL_BYTE)
            opc |= 0x400000;
        o(opc | (src1_reg << 12) | (dest_reg << 16));
      }
      break;
    case IR_OP_RETURNVOID:
      GenEpilog();
      break;
    case IR_OP_RETURNVAL:
      if (src1_reg != 0) { /* XXX: move this check to GenDataProcessingOp() */
        GenDataProcessingOp(IR_OP_ASS, 0, src1_reg, -1, (src1->r & (VT_VALMASK|VT_SYM)) == VT_CONST, src1->c.i);
      }
      GenEpilog();
      break;
    case IR_OP_FUNCPARAM:
      FuncCallParam(src2->c.i, src1_reg, (src1->r & (VT_VALMASK|VT_SYM)) == VT_CONST, src1->c.i);
      break;
    case IR_OP_FUNCPARAMVOID:
      FuncCallParam(0, 0, 0, 0);
      break;
    case IR_OP_FUNCCALLVOID:
    case IR_OP_FUNCCALLVAL:
      /* orientation on arm-gen.c:gcall_or_jmp(0) */
      if ((src1->r & (VT_VALMASK | VT_LVAL)) == VT_CONST) { /* direct call */
        if (src1->r & VT_SYM) {
	      greloc(cur_text_section, src1->sym, ind, R_ARM_PC24); /*#### linker data ####*/
        } else {
          tcc_error("IR_OP_FUNCCALL*: source operand doesn't have symbol (VT_SYM)!");
        }
        o(0xEB000000 | 0xFFFFFE);
      } else { /* indirect call */
        src1_reg = OperandInMemory(src1) ? LoadIntoReg(src1, 12) : src1->preg;
        o(0xE1A0E00F);           // mov lr,pc
        o(0xE1A0F000 | src1_reg); // mov pc,r
      }

# if 0
      if (op == IR_OP_FUNCCALLVAL) { 
        if ((dest_reg >= 0) && (dest_reg <= 3)) /* special case: destination register is in r0-r3 */
          GenDataProcessingOp(IR_OP_ASS, 14, 0, -1, 0, 0); /* save in r14 first */
        else
          GenDataProcessingOp(IR_OP_ASS, dest_reg, 0, -1, 0, 0); /* directly store return value */
      }

      /* restore argument registers (from currently generated function!) from stack */
      if (param_reg_list)
        o(0xE8BD0000 | param_reg_list); /* pop arguments */

      if ((op == IR_OP_FUNCCALLVAL) && (dest_reg >= 0) && (dest_reg <= 3))
          GenDataProcessingOp(IR_OP_ASS, dest_reg, 14, -1, 0, 0); /* store return value from previously saved r14 */
#endif
      if (op == IR_OP_FUNCCALLVAL) {
          int ret_reg = 0;
          if (src1->sym->v == TOK___aeabi_idivmod || src1->sym->v == TOK___aeabi_uidivmod)
              ret_reg = 1;
          GenDataProcessingOp(IR_OP_ASS, dest_reg, ret_reg, -1, 0, 0); /* directly store return value */
      }
      FuncCallAfter();
      break;
    }
    
    if (dest_reg == 14 && (op != IR_OP_STORE)) {
      StoreFromReg(dest, 14, 12);
    }
  }  
  if ((op != IR_OP_RETURNVOID) && (op != IR_OP_RETURNVAL)) { /* prevent redundant epilog */
    IR_addrs[i] = ind;
    GenEpilog();
  }

  /* patch branch instructions */
  for (i=1; i<irpc; i++) {
    op = IR[i].op;
    dest = &(IR[i].dest);
    if ((op == IR_OP_BRANCHIF) || (op == IR_OP_JMP)) {
      int instr_addr = IR_addrs[i];
      int target_addr = IR_addrs[dest->c.i];
      int offset = (target_addr-instr_addr-8)/4;
      uint32_t *instr = (uint32_t*)&(cur_text_section->data[instr_addr]);
      if ((offset >= 0x1000000) || (offset < -0x1000000))
        tcc_error("branch offset too large (>=16MB), sorry...");
      offset &= 0x00ffffff;
      *instr |= offset;
    }
  }
}
/********************************* debug functions **************************/
void print_quadruple(Quadruple *quad, int pc)
{
    int op = quad->op;
    printf("%04d: ", pc);
    //fflush(stdout); /* weird: without this instruction, a segfault happens! (--> avoid gv() call!) */

    /* print destination operand (respectively ops implicit destination: RETURN/PARAM/CALL) */
    switch (op) {
    case IR_OP_RETURNVOID:
    case IR_OP_RETURNVAL:
    case IR_OP_FUNCCALLVOID:
    case IR_OP_FUNCCALLVAL:
    case IR_OP_CMP:
    case IR_OP_TSTZ:
    case IR_OP_FUNCPARAMVOID:
        printf("%s ", IRS[op]);
        break;
    case IR_OP_FUNCPARAM: /* also print number of param, encoded in src2 operand */
        printf("%s%d ", IRS[op], quad->src2.c.i);
        break;
    case IR_OP_JMP: case IR_OP_BRANCHIF:
        printf("JMP to %d ", quad->dest.c.i);
        break;
    default:
        print_svalue_short(&quad->dest);
        printf(" <-- ");
    }

    /* print first source operand, if available */
    switch (op) {
    case IR_OP_RETURNVOID:
    case IR_OP_SETIF:
    case IR_OP_JMP: case IR_OP_BRANCHIF:
    case IR_OP_FUNCPARAMVOID:
        break;
    default:
        print_svalue_short(&quad->src1);
    }

    /* print second source operand, if available */
    switch (op) {
    case IR_OP_ASS:
    case IR_OP_STORE:
    case IR_OP_RETURNVOID:
    case IR_OP_RETURNVAL:
    case IR_OP_FUNCPARAM:
    case IR_OP_FUNCCALLVOID:
    case IR_OP_FUNCCALLVAL:
    case IR_OP_SETIF:
    case IR_OP_JMP: case IR_OP_BRANCHIF:
    case IR_OP_TSTZ:
    case IR_OP_FUNCPARAMVOID:
        break;
    default:
      if (op == IR_OP_CMP)
        printf(",");
      else
        printf(" %s ", IRS[quad->op]);
      print_svalue_short(&quad->src2);
    }

    /* additional information */
    if (op == IR_OP_STORE)
      printf(" [STORE]");
    else if (op == IR_OP_FUNCCALLVAL) {
      printf(" --> ");
      print_svalue_short(&quad->dest);
    } else if (op == IR_OP_SETIF) {
      printf("1 if \"");
      switch (quad->src1.c.i) {
      case TOK_EQ: printf("=="); break;
      case TOK_NE: printf("!="); break;
      case TOK_LT: printf("<S"); break;
      case TOK_GT: printf(">S"); break;
      case TOK_LE: printf("<=S"); break;
      case TOK_GE: printf(">=S"); break;
      case TOK_ULT: printf("<U"); break;
      case TOK_UGT: printf(">U"); break;
      case TOK_ULE: printf("<=U"); break;
      case TOK_UGE: printf(">=U"); break;
      }
      printf("\"");
    } else if (op == IR_OP_BRANCHIF) {
      printf("if \"");
      switch (quad->src1.c.i) {
      case TOK_EQ: printf("=="); break;
      case TOK_NE: printf("!="); break;
      case TOK_LT: printf("<S"); break;
      case TOK_GT: printf(">S"); break;
      case TOK_LE: printf("<=S"); break;
      case TOK_GE: printf(">=S"); break;
      case TOK_ULT: printf("<U"); break;
      case TOK_UGT: printf(">U"); break;
      case TOK_ULE: printf("<=U"); break;
      case TOK_UGE: printf(">=U"); break;
      }
      printf("\"");
    }
    printf("\n");
}

void print_svalue_short(SValue *sv)
{
  int val_loc = sv->r & VT_VALMASK; 
#define SPILL_MARK_BEGIN "\033[41m"
#define SPILL_MARK_END   "\033[0m"

  /* XXX: probably show ignored vregs in a special way */
  switch (val_loc) {
  case VT_CONST: 
    if (sv->r & VT_SYM)
      printf("GlobalSym(%d)", sv->sym->v);
    else
      printf("#%d", sv->c.i);
    break;
  case VT_LLOCAL: printf("VT_LLOCAL (cval=%d)", sv->c.i); break;
  //case VT_LOCAL: printf("VReg%d[stack_offset=%d]", sv->vreg, sv->c.i); break;
  case VT_LOCAL: 
    if (sv->preg != -1) { /* already register-allocated? */
       if (sv->preg & PREG_SPILLED)
         printf(SPILL_MARK_BEGIN "SpillLoc[%d]" SPILL_MARK_END, sv->c.i);
       else
         printf("R%d", sv->preg);
    } else if (sv->vreg != -1) { /* not reg-alloced, but vreg'ed? */
      printf("VReg%d", sv->vreg); 
#if 0
      printf("VReg%d[", sv->vreg); 
      int bt = sv->type.t & VT_BTYPE;
      switch (bt) {
      case VT_INT: printf("INT"); break;
      case VT_BYTE: printf("BYTE"); break;
      case VT_SHORT: printf("SHORT"); break;
      case VT_VOID: printf("VOID"); break;
      case VT_PTR: printf("PTR"); break;
      case VT_ENUM: printf("ENUM"); break;
      case VT_FUNC: printf("FUNC"); break;
      case VT_STRUCT: printf("STRUCT"); break;
      case VT_BOOL: printf("BOOL"); break;
      default:
         printf("OTHER=%d", bt);
      }
      if (sv->r & VT_LVAL) printf(",LVAL");
      if ((sv->r & VT_VALMASK) == VT_LOCAL) printf(",VT_LOCAL");
      printf("]");
#endif

    } else if (!(sv->r & VT_LVAL)) { /* no LVAL, is just an address */
      printf("Addr[StackLoc[%d]]", sv->c.i);
    } else { /* fixed location on stack */
      printf("StackLoc[%d]", sv->c.i);
    }
    break;
  case VT_CMP: printf("VT_CMP"); break;
  case VT_JMP: printf("VT_JMP"); break;
  case VT_JMPI: printf("VT_JMPI"); break;
  default: /* must be temporary vreg */
    if (sv->preg == -1) {
      printf("VReg%d", sv->vreg); 
#if 0
      printf("VReg%d[", sv->vreg); 
      int bt = sv->type.t & VT_BTYPE;
      switch (bt) {
      case VT_INT: printf("INT"); break;
      case VT_BYTE: printf("BYTE"); break;
      case VT_SHORT: printf("SHORT"); break;
      case VT_VOID: printf("VOID"); break;
      case VT_PTR: printf("PTR"); break;
      case VT_ENUM: printf("ENUM"); break;
      case VT_FUNC: printf("FUNC"); break;
      case VT_STRUCT: printf("STRUCT"); break;
      case VT_BOOL: printf("BOOL"); break;
      default:
         printf("OTHER=%d", bt);
      }
      if (sv->r & VT_LVAL) printf(",LVAL");
      printf("]");
#endif
      if (sv->r & VT_LVAL) printf("***DEREF***");
    } else {
       if (sv->preg & PREG_SPILLED)
         printf(SPILL_MARK_BEGIN "SpillLoc[%d]" SPILL_MARK_END, sv->c.i);
       else
         printf("R%d", sv->preg);
       if (sv->r & VT_LVAL) printf("***DEREF***");
    }
    break;
  }
}

void print_svalue_info(SValue *sv)
{
  int btype;
  int val_loc;

  printf("BasicType: ");
  btype = sv->type.t & VT_BTYPE;
  switch (btype) {
  case VT_INT: printf("VT_INT"); break;
  case VT_BYTE: printf("VT_BYTE"); break;
  case VT_SHORT: printf("VT_SHORT"); break;
  case VT_VOID: printf("VT_VOID"); break;
  case VT_PTR: printf("VT_PTR"); break;
  case VT_ENUM: printf("VT_ENUM"); break;
  case VT_FUNC: printf("VT_FUNC"); break;
  case VT_STRUCT: printf("VT_STRUCT"); break;
  }
  printf(" | TypeFlags: ");
  if (sv->type.t & VT_UNSIGNED) printf("VT_UNSIGNED,");
  if (sv->type.t & VT_ARRAY) printf("VT_ARRAY,");
  if (sv->type.t & VT_BITFIELD) printf("VT_BITFIELD,");
  if (sv->type.t & VT_CONSTANT) printf("VT_CONSTANT,");
  if (sv->type.t & VT_VOLATILE) printf("VT_VOLATILE,");
  if (sv->type.t & VT_SIGNED) printf("VT_SIGNED,");

  printf(" | ValueLocation: ");
  val_loc = sv->r & VT_VALMASK;
  switch (val_loc) {
  case VT_CONST: printf("VT_CONST (cval=%d)", sv->c.i); break;
  case VT_LLOCAL: printf("VT_LLOCAL (cval=%d)", sv->c.i); break;
  case VT_LOCAL: printf("VT_LOCAL (cval=%d)", sv->c.i); printf(", VReg%d", sv->vreg); break;
  case VT_CMP: printf("VT_CMP"); break;
  case VT_JMP: printf("VT_JMP"); break;
  case VT_JMPI: printf("VT_JMPI"); break;
  default: /* must be temporary vreg */
	printf("VReg%d", sv->vreg); break;
  }

  printf(" | ValueFlags: ");
  if (sv->r & VT_REF) printf("VT_REF,");
  if (sv->r & VT_LVAL) printf("VT_LVAL,");
  if (sv->r & VT_SYM) printf("VT_SYM,");
} 

