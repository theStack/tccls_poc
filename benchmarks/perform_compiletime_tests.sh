#!/bin/bash

TCCLS="/home/pi/tccls_poc/tcc"
GCC_FLAGS="-march=armv4"

N_MEASUREMENTS="15"

# argument 1: command for measurement
# return value: measured time in seconds, stored in $MEASURED_TIME
measure_time_for_command() {
    # probably pass "-f%U" if we want to measure only user time?
    MEASURED_TIME=$(/usr/bin/time -f%e $1 2>&1 >/dev/null)
}

# argument 1: command for measurement series
# return value: median measured time in seconds, stored in $RESULT
series_of_measurements() {
    echo "---> Measurement for \"$1\""
    VALUES=()
    echo -en "\t"
    for (( i=1; i<=$N_MEASUREMENTS; i++ )); do
        measure_time_for_command "$1"
        echo -n "$MEASURED_TIME "
        VALUES=(${VALUES[*]} $MEASURED_TIME)
    done
    STDIFS=$IFS
    IFS=$'\n' VALUES_SORTED=($(sort -n <<<"${VALUES[*]}"))
    IFS=$STDIFS
    N=${#VALUES_SORTED[@]}
    MID=$((N/2))
    RESULT=${VALUES_SORTED[$MID]}
    
    echo
    echo "[ Sorted series of measurements: ${VALUES_SORTED[@]} ]"
    echo -e "\tMedian exec. time: ---> \e[1;92m $RESULT \e[0m <---"
}

# argument 1: execution time of tccls
# argument 2: execution time of original tcc
show_speedup() {
    SPEED_UP=$(echo "scale=4; (($2 / $1)-1)*100" | bc -l)
    echo -e "speedup from tcc orig => tccls: \e[1;34;40m  +$SPEED_UP%  \e[0m"
}

echo ======================================
echo ===== Test 0: Collatz Conjecture =====
echo ======================================
cd own
series_of_measurements "$TCCLS -o col_ls collatz.c"
series_of_measurements "tcc -o col_orig collatz.c"
series_of_measurements "gcc -o col_gccO0 collatz.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o col_gccO1 collatz.c -O1 $GCC_FLAGS"
rm -f col_*
cd -

echo ======================================
echo ===== Test 1: MiBench Bitcounts ======
echo ======================================
echo NOT READY YET, but too small fiels to yield meaningful results anyway...
#cd mibench/automotive/bitcount
#series_of_measurements "$TCCLS -o bitcnt_ls bitcnt_1.c bitcnt_2.c bitcnt_3.c bitcnt_4.c bitcnts.c"
#series_of_measurements "tcc -o bitcnt_orig bitcnt_1.c bitcnt_2.c bitcnt_3.c bitcnt_4.c bitcnts.c"
#series_of_measurements "gcc -o bitcnt_gccO0 bitcnt_1.c bitcnt_2.c bitcnt_3.c bitcnt_4.c bitcnts.c -O0 $GCC_FLAGS"
#series_of_measurements "gcc -o bitcnt_gccO1 bitcnt_1.c bitcnt_2.c bitcnt_3.c bitcnt_4.c bitcnts.c -O1 $GCC_FLAGS"
#cd -

echo ===============================
echo ===== Test 2: MiBench SHA =====
echo ===============================
cd mibench/security/sha
make clean
series_of_measurements "$TCCLS -o sha_ls sha.c sha_driver.c"
series_of_measurements "tcc -o sha_orig sha.c sha_driver.c"
series_of_measurements "gcc -o sha_gccO0 sha.c sha_driver.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o sha_gccO1 sha.c sha_driver.c -O1 $GCC_FLAGS"
cd -

echo ========================================
echo ===== Test 3: MiBench stringsearch =====
echo ========================================
cd mibench/office/stringsearch/
series_of_measurements "$TCCLS -o search_ls -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c"
series_of_measurements "tcc -o search_orig -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c"
series_of_measurements "gcc -o search_gccO0 -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o search_gccO1 -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c -O1 $GCC_FLAGS"
rm -f search_*
cd -

echo =================================
echo ===== Test 4: MiBench adpcm =====
echo =================================
cd mibench/telecomm/adpcm/
echo "------ Coder -----"
series_of_measurements "$TCCLS -o rawcaudio_ls src/rawcaudio.c src/adpcm.c"
series_of_measurements "tcc -o rawcaudio_orig src/rawcaudio.c src/adpcm.c"
series_of_measurements "gcc -o rawcaudio_gccO0 src/rawcaudio.c src/adpcm.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o rawcaudio_gccO1 src/rawcaudio.c src/adpcm.c -O1 $GCC_FLAGS"

echo "----- Decoder -----"
series_of_measurements "$TCCLS -o rawdaudio_ls src/rawdaudio.c src/adpcm.c"
series_of_measurements "tcc -o rawdaudio_orig src/rawdaudio.c src/adpcm.c"
series_of_measurements "gcc -o rawdaudio_gccO0 src/rawdaudio.c src/adpcm.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o rawdaudio_gccO1 src/rawdaudio.c src/adpcm.c -O1 $GCC_FLAGS"
cd -

echo ====================================
echo ===== Test 5: MiBench dijkstra =====
echo ====================================
cd mibench/network/dijkstra/
echo "----- Dijkstra orig -----"
series_of_measurements "$TCCLS -o dl_ls dijkstra_large.c"
series_of_measurements "tcc -o dl_orig dijkstra_large.c"
series_of_measurements "gcc -o dl_gccO0 dijkstra_large.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o dl_gccO1 dijkstra_large.c -O1 $GCC_FLAGS"

echo "----- Dijkstra opt. -----"
rm -f dl_*
series_of_measurements "$TCCLS -o dl_ls dijkstra_opt.c"
series_of_measurements "tcc -o dl_orig dijkstra_opt.c"
series_of_measurements "gcc -o dl_gccO0 dijkstra_opt.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o dl_gccO1 dijkstra_opt.c -O1 $GCC_FLAGS"
cd -

echo ================================
echo ===== Test n: bzip2-0.1pl2 =====
echo ================================
cd other_interesting
series_of_measurements "$TCCLS -o bzip_ls bzip2-0.1pl2.c"
series_of_measurements "tcc -o bzip_orig bzip2-0.1pl2.c"
series_of_measurements "gcc -o bzip_gccO0 bzip2-0.1pl2.c -O0 $GCC_FLAGS"
series_of_measurements "gcc -o bzip_gccO1 bzip2-0.1pl2.c -O1 $GCC_FLAGS"

echo "===== special test for measuring execution time for TCCLS phases: (called 5x for average) ====="
$TCCLS -o bzip_ls bzip2-0.1pl2.c -bench
$TCCLS -o bzip_ls bzip2-0.1pl2.c -bench
$TCCLS -o bzip_ls bzip2-0.1pl2.c -bench
$TCCLS -o bzip_ls bzip2-0.1pl2.c -bench
$TCCLS -o bzip_ls bzip2-0.1pl2.c -bench
cd -
exit

