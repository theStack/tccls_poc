#!/bin/bash

TCCLS="/home/pi/tccls_poc/tcc"
GCC_FLAGS="-march=armv4 -mno-thumb -mno-thumb-interwork"

N_MEASUREMENTS="15"
COLLATZ_MAX_NUM="2000000"
BITCNT_ITERATIONS="20000000"
SHA_TESTFILE="/home/pi/gcc-4.5.0.tar.gz"
BZIP2_TESTFILE="/home/pi/busybox-0.60.5.tar"

# argument 1: command for measurement
# return value: measured time in seconds, stored in $MEASURED_TIME
measure_time_for_command() {
    # probably pass "-f%U" if we want to measure only user time?
    MEASURED_TIME=$(/usr/bin/time -f%e $1 2>&1 >/dev/null)
}

# argument 1: command for measurement series
# return value: median measured time in seconds, stored in $RESULT
series_of_measurements() {
    echo "---> Measurement for \"$1\""
    VALUES=()
    echo -en "\t"
    for (( i=1; i<=$N_MEASUREMENTS; i++ )); do
        measure_time_for_command "$1"
        echo -n "$MEASURED_TIME "
        VALUES=(${VALUES[*]} $MEASURED_TIME)
    done
    STDIFS=$IFS
    IFS=$'\n' VALUES_SORTED=($(sort -n <<<"${VALUES[*]}"))
    IFS=$STDIFS
    N=${#VALUES_SORTED[@]}
    MID=$((N/2))
    RESULT=${VALUES_SORTED[$MID]}
    
    echo
    echo "[ Sorted series of measurements: ${VALUES_SORTED[@]} ]"
    echo -e "\tMedian exec. time: ---> \e[1;92m $RESULT \e[0m <---"
}

# argument 1: execution time of tccls
# argument 2: execution time of original tcc
show_speedup() {
    SPEED_UP=$(echo "scale=4; (($2 / $1)-1)*100" | bc -l)
    echo -e "speedup from tcc orig => tccls: \e[1;34;40m  +$SPEED_UP%  \e[0m"
}

echo ======================================
echo ===== Test 0: Collatz Conjecture =====
echo ======================================
cd own
$TCCLS -o col_ls collatz.c > /dev/null
tcc -o col_orig collatz.c > /dev/null
gcc -o col_gccO0 collatz.c -O0 $GCC_FLAGS
gcc -o col_gccO1 collatz.c -O1 $GCC_FLAGS
echo -n "tccls "
series_of_measurements "./col_ls $COLLATZ_MAX_NUM"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./col_orig $COLLATZ_MAX_NUM"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./col_gccO0 $COLLATZ_MAX_NUM"
echo -n "gcc -O1 "
series_of_measurements "./col_gccO1 $COLLATZ_MAX_NUM"
cd -

echo ======================================
echo ===== Test 1: MiBench Bitcounts ======
echo ======================================
echo "ATTENTION: This benchmark is done with an integrated test program that uses the clock() routine, no average values here!"
cd mibench/automotive/bitcount
$TCCLS -c bitcnt_1.c > /dev/null
$TCCLS -c bitcnt_2.c > /dev/null
$TCCLS -c bitcnt_3.c > /dev/null
$TCCLS -c bitcnt_4.c > /dev/null
gcc -o bitcnt_ls bitcnt_1.o bitcnt_2.o bitcnt_3.o bitcnt_4.o bitcnts.c
tcc -c bitcnt_1.c > /dev/null
tcc -c bitcnt_2.c > /dev/null
tcc -c bitcnt_3.c > /dev/null
tcc -c bitcnt_4.c > /dev/null
gcc -o bitcnt_orig bitcnt_1.o bitcnt_2.o bitcnt_3.o bitcnt_4.o bitcnts.c
gcc -c bitcnt_1.c -O0 $GCC_FLAGS
gcc -c bitcnt_2.c -O0 $GCC_FLAGS
gcc -c bitcnt_3.c -O0 $GCC_FLAGS
gcc -c bitcnt_4.c -O0 $GCC_FLAGS
gcc -o bitcnt_gccO0 bitcnt_1.o bitcnt_2.o bitcnt_3.o bitcnt_4.o bitcnts.c
gcc -c bitcnt_1.c -O1 $GCC_FLAGS
gcc -c bitcnt_2.c -O1 $GCC_FLAGS
gcc -c bitcnt_3.c -O1 $GCC_FLAGS
gcc -c bitcnt_4.c -O1 $GCC_FLAGS
gcc -o bitcnt_gccO1 bitcnt_1.o bitcnt_2.o bitcnt_3.o bitcnt_4.o bitcnts.c
echo "--- tccls ---"
./bitcnt_ls $BITCNT_ITERATIONS
echo "--- tcc org---"
./bitcnt_orig $BITCNT_ITERATIONS
echo "--- gcc -O0 ---"
./bitcnt_gccO0 $BITCNT_ITERATIONS
echo "--- gcc -O1 ---"
./bitcnt_gccO1 $BITCNT_ITERATIONS
cd -

echo ===============================
echo ===== Test 2: MiBench SHA =====
echo ===============================
cd mibench/security/sha
make clean
$TCCLS -o sha_ls sha.c sha_driver.c > /dev/null
tcc -o sha_orig sha.c sha_driver.c > /dev/null
gcc -o sha_gccO0 sha.c sha_driver.c -O0 $GCC_FLAGS
gcc -o sha_gccO1 sha.c sha_driver.c -O1 $GCC_FLAGS
echo -n "tccls "
series_of_measurements "./sha_ls $SHA_TESTFILE"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./sha_orig $SHA_TESTFILE"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./sha_gccO0 $SHA_TESTFILE"
echo -n "gcc -O1 "
series_of_measurements "./sha_gccO1 $SHA_TESTFILE"
cd -

echo ========================================
echo ===== Test 3: MiBench stringsearch =====
echo ========================================
cd mibench/office/stringsearch/
$TCCLS -o search_ls -DSEARCH_PBM pbm_fileintostring.c > /dev/null
tcc -o search_orig -DSEARCH_PBM pbm_fileintostring.c > /dev/null
gcc -o search_gccO0 -DSEARCH_PBM pbm_fileintostring.c -O0 $GCC_FLAGS
gcc -o search_gccO1 -DSEARCH_PBM pbm_fileintostring.c -O1 $GCC_FLAGS
echo "-----> Subtest: PBM search <-----"
echo -n "tccls "
series_of_measurements "./search_ls ./badwords/badwords.txt pg4300.txt"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./search_orig ./badwords/badwords.txt pg4300.txt"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./search_gccO0 ./badwords/badwords.txt pg4300.txt"
echo -n "gcc -O1 "
series_of_measurements "./search_gccO1 ./badwords/badwords.txt pg4300.txt"
rm -f search_*

$TCCLS -o search_ls -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c > /dev/null
tcc -o search_orig -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c > /dev/null
gcc -o search_gccO0 -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c -O0 $GCC_FLAGS
gcc -o search_gccO1 -DSEARCH_BMH pbm_fileintostring.c bmhsrch.c -O1 $GCC_FLAGS
echo "-----> Subtest: BMH search <-----"
echo -n "tccls "
series_of_measurements "./search_ls ./badwords/badwords.txt pg4300.txt"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./search_orig ./badwords/badwords.txt pg4300.txt"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./search_gccO0 ./badwords/badwords.txt pg4300.txt"
echo -n "gcc -O1 "
series_of_measurements "./search_gccO1 ./badwords/badwords.txt pg4300.txt"
rm -f search_*

cd -

echo =================================
echo ===== Test 4: MiBench adpcm =====
echo =================================
cd mibench/telecomm/adpcm/
$TCCLS -o rawcaudio_ls src/rawcaudio.c src/adpcm.c > /dev/null
$TCCLS -o rawdaudio_ls src/rawdaudio.c src/adpcm.c > /dev/null
tcc -o rawcaudio_orig src/rawcaudio.c src/adpcm.c > /dev/null
tcc -o rawdaudio_orig src/rawdaudio.c src/adpcm.c > /dev/null
gcc -o rawcaudio_gccO0 src/rawcaudio.c src/adpcm.c -O0 $GCC_FLAGS
gcc -o rawdaudio_gccO0 src/rawdaudio.c src/adpcm.c -O0 $GCC_FLAGS
gcc -o rawcaudio_gccO1 src/rawcaudio.c src/adpcm.c -O1 $GCC_FLAGS
gcc -o rawdaudio_gccO1 src/rawdaudio.c src/adpcm.c -O1 $GCC_FLAGS
echo "-----> Subtest: adpcm code <-----"
echo -n "tccls "
rm -f output_large.adpcm
series_of_measurements "./rawcaudio_ls data/large.pcm output_large.adpcm"
RESULT_LS=$RESULT
sha1sum output_large.adpcm
echo -n "tcc org "
series_of_measurements "./rawcaudio_orig data/large.pcm output_large.adpcm"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./rawcaudio_gccO0 data/large.pcm output_large.adpcm"
echo -n "gcc -O1 "
rm -f output_large.adpcm
series_of_measurements "./rawcaudio_gccO1 data/large.pcm output_large.adpcm"
sha1sum output_large.adpcm

echo "-----> Subtest: adpcm decode <-----"
echo -n "tccls "
rm -f output_large.pcm
series_of_measurements "./rawdaudio_ls data/large.adpcm output_large.pcm"
RESULT_LS=$RESULT
sha1sum output_large.pcm
echo -n "tcc org "
series_of_measurements "./rawdaudio_orig data/large.adpcm output_large.pcm"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./rawdaudio_gccO0 data/large.adpcm output_large.pcm"
echo -n "gcc -O1 "
rm -f output_large.pcm
series_of_measurements "./rawdaudio_gccO1 data/large.adpcm output_large.pcm"
sha1sum output_large.pcm
cd -

echo ====================================
echo ===== Test 5: MiBench dijkstra =====
echo ====================================
cd mibench/network/dijkstra/
$TCCLS -o dl_ls dijkstra_large.c > /dev/null
tcc -o dl_orig dijkstra_large.c > /dev/null
gcc -o dl_gccO0 dijkstra_large.c -O0 $GCC_FLAGS
gcc -o dl_gccO1 dijkstra_large.c -O1 $GCC_FLAGS
echo "-----> Subtest: dijkstra original <-----"
echo -n "tccls "
series_of_measurements "./dl_ls input.dat"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./dl_orig input.dat"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./dl_gccO0 input.dat"
echo -n "gcc -O1 "
series_of_measurements "./dl_gccO1 input.dat"

rm -f dl_*
$TCCLS -o dl_ls dijkstra_opt.c > /dev/null
tcc -o dl_orig dijkstra_opt.c > /dev/null
gcc -o dl_gccO0 dijkstra_opt.c -O0 $GCC_FLAGS
gcc -o dl_gccO1 dijkstra_opt.c -O1 $GCC_FLAGS
echo "-----> Subtest: dijkstra optimized (global vars => local vars) <-----"
echo -n "tccls "
series_of_measurements "./dl_ls input.dat"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./dl_orig input.dat"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./dl_gccO0 input.dat"
echo -n "gcc -O1 "
series_of_measurements "./dl_gccO1 input.dat"
cd -

echo ================================
echo ===== Test n: bzip2-0.1pl2 =====
echo ================================
cd other_interesting
$TCCLS -o bzip_ls bzip2-0.1pl2.c > /dev/null
tcc -o bzip_orig bzip2-0.1pl2.c > /dev/null
gcc -o bzip_gccO0 bzip2-0.1pl2.c -O0 $GCC_FLAGS
gcc -o bzip_gccO1 bzip2-0.1pl2.c -O1 $GCC_FLAGS
rm -f $BZIP2_TESTFILE.bz2
echo "-----> Subtest: bzip2 Inflate <-----"
echo -n "tccls "
series_of_measurements "./bzip_ls -k -f $BZIP2_TESTFILE"
rm $BZIP2_TESTFILE.bz2
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./bzip_orig -k -f $BZIP2_TESTFILE"
rm $BZIP2_TESTFILE.bz2
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./bzip_gccO0 -k -f $BZIP2_TESTFILE"
rm $BZIP2_TESTFILE.bz2
echo -n "gcc -O1 "
series_of_measurements "./bzip_gccO1 -k -f $BZIP2_TESTFILE"

echo "-----> Subtest: bzip2 Test <-----"
echo -n "tccls "
series_of_measurements "./bzip_ls -t $BZIP2_TESTFILE.bz2"
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./bzip_orig -t $BZIP2_TESTFILE.bz2"
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./bzip_gccO0 -t $BZIP2_TESTFILE.bz2"
echo -n "gcc -O1 "
series_of_measurements "./bzip_gccO1 -t $BZIP2_TESTFILE.bz2"

rm $BZIP2_TESTFILE
echo "-----> Subtest: bzip2 Deflate <-----"
echo -n "tccls "
series_of_measurements "./bzip_ls -k -d $BZIP2_TESTFILE.bz2"
rm $BZIP2_TESTFILE
RESULT_LS=$RESULT
echo -n "tcc org "
series_of_measurements "./bzip_orig -k -d $BZIP2_TESTFILE.bz2"
rm $BZIP2_TESTFILE
show_speedup $RESULT_LS $RESULT
echo -n "gcc -O0 "
series_of_measurements "./bzip_gccO0 -k -d $BZIP2_TESTFILE.bz2"
rm $BZIP2_TESTFILE
echo -n "gcc -O1 "
series_of_measurements "./bzip_gccO1 -k -d $BZIP2_TESTFILE.bz2"

cd -
exit

