#!/bin/sh
echo Dhrystone for gcc -O0:
gcc -o dhry dhry_1.c dhry_2.c cpuidc.c -O0 -lrt -march=armv4
./dhry n | grep "MIPS rating"
echo

echo Dhrystone for gcc -O1:
gcc -o dhry dhry_1.c dhry_2.c cpuidc.c -O1 -lrt -march=armv4
./dhry n | grep "MIPS rating"
echo

echo Dhrystone for tcc:
tcc -o dhry dhry_1.c dhry_2.c cpuidc.c -lrt
./dhry n | grep "MIPS rating"
