
bitcnt_1.o:     file format elf32-littlearm


Disassembly of section .text:

00000000 <__bswap_16>:
   0:	e1a0c00d 	mov	ip, sp
   4:	e92d0003 	push	{r0, r1}
   8:	e92d5800 	push	{fp, ip, lr}
   c:	e1a0b00d 	mov	fp, sp
  10:	e1a00000 	nop			; (mov r0, r0)
  14:	e1db00bc 	ldrh	r0, [fp, #12]
  18:	e1a00440 	asr	r0, r0, #8
  1c:	e20000ff 	and	r0, r0, #255	; 0xff
  20:	e1db10bc 	ldrh	r1, [fp, #12]
  24:	e20110ff 	and	r1, r1, #255	; 0xff
  28:	e1a01401 	lsl	r1, r1, #8
  2c:	e1800001 	orr	r0, r0, r1
  30:	e59f1000 	ldr	r1, [pc]	; 38 <__bswap_16+0x38>
  34:	ea000000 	b	3c <__bswap_16+0x3c>
  38:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
  3c:	e0000001 	and	r0, r0, r1
  40:	e1a00000 	nop			; (mov r0, r0)
  44:	e89ba800 	ldm	fp, {fp, sp, pc}

00000048 <__bswap_32>:
  48:	e1a0c00d 	mov	ip, sp
  4c:	e92d0003 	push	{r0, r1}
  50:	e92d5800 	push	{fp, ip, lr}
  54:	e1a0b00d 	mov	fp, sp
  58:	e1a00000 	nop			; (mov r0, r0)
  5c:	e59b000c 	ldr	r0, [fp, #12]
  60:	e20004ff 	and	r0, r0, #-16777216	; 0xff000000
  64:	e1a00c20 	lsr	r0, r0, #24
  68:	e59b100c 	ldr	r1, [fp, #12]
  6c:	e20118ff 	and	r1, r1, #16711680	; 0xff0000
  70:	e1a01421 	lsr	r1, r1, #8
  74:	e1800001 	orr	r0, r0, r1
  78:	e59b100c 	ldr	r1, [fp, #12]
  7c:	e2011cff 	and	r1, r1, #65280	; 0xff00
  80:	e1a01401 	lsl	r1, r1, #8
  84:	e1800001 	orr	r0, r0, r1
  88:	e59b100c 	ldr	r1, [fp, #12]
  8c:	e20110ff 	and	r1, r1, #255	; 0xff
  90:	e1a01c01 	lsl	r1, r1, #24
  94:	e1800001 	orr	r0, r0, r1
  98:	e1a00000 	nop			; (mov r0, r0)
  9c:	e89ba800 	ldm	fp, {fp, sp, pc}

000000a0 <bit_count>:
  a0:	e1a0c00d 	mov	ip, sp
  a4:	e92d0003 	push	{r0, r1}
  a8:	e92d5800 	push	{fp, ip, lr}
  ac:	e1a0b00d 	mov	fp, sp
  b0:	e24bd004 	sub	sp, fp, #4
  b4:	e3a00000 	mov	r0, #0
  b8:	e50b0004 	str	r0, [fp, #-4]
  bc:	e59b000c 	ldr	r0, [fp, #12]
  c0:	e3300000 	teq	r0, #0
  c4:	0a00000b 	beq	f8 <bit_count+0x58>
  c8:	e51b0004 	ldr	r0, [fp, #-4]
  cc:	e1a01000 	mov	r1, r0
  d0:	e2800001 	add	r0, r0, #1
  d4:	e50b0004 	str	r0, [fp, #-4]
  d8:	e59b000c 	ldr	r0, [fp, #12]
  dc:	e2400001 	sub	r0, r0, #1
  e0:	e59b100c 	ldr	r1, [fp, #12]
  e4:	e0010000 	and	r0, r1, r0
  e8:	e58b000c 	str	r0, [fp, #12]
  ec:	e3a01000 	mov	r1, #0
  f0:	e1510000 	cmp	r1, r0
  f4:	1afffff3 	bne	c8 <bit_count+0x28>
  f8:	e51b0004 	ldr	r0, [fp, #-4]
  fc:	e1a00000 	nop			; (mov r0, r0)
 100:	e89ba800 	ldm	fp, {fp, sp, pc}
