
collatz.o:     file format elf32-littlearm


Disassembly of section .text:

00000000 <__bswap_16>:
   0:	e1a0c00d 	mov	ip, sp
   4:	e92d0003 	push	{r0, r1}
   8:	e92d5800 	push	{fp, ip, lr}
   c:	e1a0b00d 	mov	fp, sp
  10:	e1a00000 	nop			; (mov r0, r0)
  14:	e1db00bc 	ldrh	r0, [fp, #12]
  18:	e1a00440 	asr	r0, r0, #8
  1c:	e20000ff 	and	r0, r0, #255	; 0xff
  20:	e1db10bc 	ldrh	r1, [fp, #12]
  24:	e20110ff 	and	r1, r1, #255	; 0xff
  28:	e1a01401 	lsl	r1, r1, #8
  2c:	e1800001 	orr	r0, r0, r1
  30:	e59f1000 	ldr	r1, [pc]	; 38 <__bswap_16+0x38>
  34:	ea000000 	b	3c <__bswap_16+0x3c>
  38:	0000ffff 	strdeq	pc, [r0], -pc	; <UNPREDICTABLE>
  3c:	e0000001 	and	r0, r0, r1
  40:	e1a00000 	nop			; (mov r0, r0)
  44:	e89ba800 	ldm	fp, {fp, sp, pc}

00000048 <__bswap_32>:
  48:	e1a0c00d 	mov	ip, sp
  4c:	e92d0003 	push	{r0, r1}
  50:	e92d5800 	push	{fp, ip, lr}
  54:	e1a0b00d 	mov	fp, sp
  58:	e1a00000 	nop			; (mov r0, r0)
  5c:	e59b000c 	ldr	r0, [fp, #12]
  60:	e20004ff 	and	r0, r0, #-16777216	; 0xff000000
  64:	e1a00c20 	lsr	r0, r0, #24
  68:	e59b100c 	ldr	r1, [fp, #12]
  6c:	e20118ff 	and	r1, r1, #16711680	; 0xff0000
  70:	e1a01421 	lsr	r1, r1, #8
  74:	e1800001 	orr	r0, r0, r1
  78:	e59b100c 	ldr	r1, [fp, #12]
  7c:	e2011cff 	and	r1, r1, #65280	; 0xff00
  80:	e1a01401 	lsl	r1, r1, #8
  84:	e1800001 	orr	r0, r0, r1
  88:	e59b100c 	ldr	r1, [fp, #12]
  8c:	e20110ff 	and	r1, r1, #255	; 0xff
  90:	e1a01c01 	lsl	r1, r1, #24
  94:	e1800001 	orr	r0, r0, r1
  98:	e1a00000 	nop			; (mov r0, r0)
  9c:	e89ba800 	ldm	fp, {fp, sp, pc}

000000a0 <collatz>:
  a0:	e1a0c00d 	mov	ip, sp
  a4:	e92d0003 	push	{r0, r1}
  a8:	e92d5800 	push	{fp, ip, lr}
  ac:	e1a0b00d 	mov	fp, sp
  b0:	e24bd004 	sub	sp, fp, #4
  b4:	e3a00001 	mov	r0, #1
  b8:	e50b0004 	str	r0, [fp, #-4]
  bc:	e59b000c 	ldr	r0, [fp, #12]
  c0:	e3500001 	cmp	r0, #1
  c4:	0a000013 	beq	118 <collatz+0x78>
  c8:	e59b000c 	ldr	r0, [fp, #12]
  cc:	e2000001 	and	r0, r0, #1
  d0:	e3300000 	teq	r0, #0
  d4:	0a000007 	beq	f8 <collatz+0x58>
  d8:	e59b000c 	ldr	r0, [fp, #12]
  dc:	e3a01003 	mov	r1, #3
  e0:	e0000091 	mul	r0, r1, r0
  e4:	e58b000c 	str	r0, [fp, #12]
  e8:	e59b000c 	ldr	r0, [fp, #12]
  ec:	e2800001 	add	r0, r0, #1
  f0:	e58b000c 	str	r0, [fp, #12]
  f4:	ea000002 	b	104 <collatz+0x64>
  f8:	e59b000c 	ldr	r0, [fp, #12]
  fc:	e1a000a0 	lsr	r0, r0, #1
 100:	e58b000c 	str	r0, [fp, #12]
 104:	e51b0004 	ldr	r0, [fp, #-4]
 108:	e1a01000 	mov	r1, r0
 10c:	e2800001 	add	r0, r0, #1
 110:	e50b0004 	str	r0, [fp, #-4]
 114:	eaffffe8 	b	bc <collatz+0x1c>
 118:	e51b0004 	ldr	r0, [fp, #-4]
 11c:	e1a00000 	nop			; (mov r0, r0)
 120:	e89ba800 	ldm	fp, {fp, sp, pc}

00000124 <main>:
 124:	e1a0c00d 	mov	ip, sp
 128:	e92d0003 	push	{r0, r1}
 12c:	e92d5800 	push	{fp, ip, lr}
 130:	e1a0b00d 	mov	fp, sp
 134:	e24bd014 	sub	sp, fp, #20
 138:	e3a00000 	mov	r0, #0
 13c:	e50b000c 	str	r0, [fp, #-12]
 140:	e59b000c 	ldr	r0, [fp, #12]
 144:	e3500002 	cmp	r0, #2
 148:	0a000001 	beq	154 <main+0x30>
 14c:	e3a00001 	mov	r0, #1
 150:	ea000027 	b	1f4 <main+0xd0>
 154:	e59b0010 	ldr	r0, [fp, #16]
 158:	e2800004 	add	r0, r0, #4
 15c:	e3a0200a 	mov	r2, #10
 160:	e3a01000 	mov	r1, #0
 164:	e50b0010 	str	r0, [fp, #-16]
 168:	e51be010 	ldr	lr, [fp, #-16]
 16c:	e59e0000 	ldr	r0, [lr]
 170:	ebfffffe 	bl	0 <strtol>
 174:	e50b0008 	str	r0, [fp, #-8]
 178:	e51b1008 	ldr	r1, [fp, #-8]
 17c:	e59f0000 	ldr	r0, [pc]	; 184 <main+0x60>
 180:	ea000000 	b	188 <main+0x64>
 184:	00000000 	andeq	r0, r0, r0
 188:	ebfffffe 	bl	0 <printf>
 18c:	e3a00001 	mov	r0, #1
 190:	e50b0004 	str	r0, [fp, #-4]
 194:	e51b0004 	ldr	r0, [fp, #-4]
 198:	e51b1008 	ldr	r1, [fp, #-8]
 19c:	e1500001 	cmp	r0, r1
 1a0:	aa00000b 	bge	1d4 <main+0xb0>
 1a4:	ea000004 	b	1bc <main+0x98>
 1a8:	e51b0004 	ldr	r0, [fp, #-4]
 1ac:	e1a01000 	mov	r1, r0
 1b0:	e2800001 	add	r0, r0, #1
 1b4:	e50b0004 	str	r0, [fp, #-4]
 1b8:	eafffff5 	b	194 <main+0x70>
 1bc:	e51b0004 	ldr	r0, [fp, #-4]
 1c0:	ebfffffe 	bl	a0 <collatz>
 1c4:	e51b100c 	ldr	r1, [fp, #-12]
 1c8:	e0810000 	add	r0, r1, r0
 1cc:	e50b000c 	str	r0, [fp, #-12]
 1d0:	eafffff4 	b	1a8 <main+0x84>
 1d4:	e51b200c 	ldr	r2, [fp, #-12]
 1d8:	e51b1008 	ldr	r1, [fp, #-8]
 1dc:	e59f0000 	ldr	r0, [pc]	; 1e4 <main+0xc0>
 1e0:	ea000000 	b	1e8 <main+0xc4>
 1e4:	00000000 	andeq	r0, r0, r0
 1e8:	ebfffffe 	bl	0 <printf>
 1ec:	e3a00000 	mov	r0, #0
 1f0:	e1a00000 	nop			; (mov r0, r0)
 1f4:	e89ba800 	ldm	fp, {fp, sp, pc}
