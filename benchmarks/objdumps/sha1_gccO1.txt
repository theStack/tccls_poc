
sha.o:     file format elf32-littlearm


Disassembly of section .text:

00000000 <sha_transform>:
   0:	e92d0ff0 	push	{r4, r5, r6, r7, r8, r9, sl, fp}
   4:	e24dde15 	sub	sp, sp, #336	; 0x150
   8:	e2802018 	add	r2, r0, #24
   c:	e28d100c 	add	r1, sp, #12
  10:	e28d404c 	add	r4, sp, #76	; 0x4c
  14:	e1a03001 	mov	r3, r1
  18:	e5b2c004 	ldr	ip, [r2, #4]!
  1c:	e5a3c004 	str	ip, [r3, #4]!
  20:	e1530004 	cmp	r3, r4
  24:	1afffffb 	bne	18 <sha_transform+0x18>
  28:	e28d3010 	add	r3, sp, #16
  2c:	e3a02000 	mov	r2, #0
  30:	e5934034 	ldr	r4, [r3, #52]	; 0x34
  34:	e593c020 	ldr	ip, [r3, #32]
  38:	e024c00c 	eor	ip, r4, ip
  3c:	e2826010 	add	r6, r2, #16
  40:	e1a0500d 	mov	r5, sp
  44:	e7964005 	ldr	r4, [r6, r5]
  48:	e02cc004 	eor	ip, ip, r4
  4c:	e5934008 	ldr	r4, [r3, #8]
  50:	e02cc004 	eor	ip, ip, r4
  54:	e583c040 	str	ip, [r3, #64]	; 0x40
  58:	e2822004 	add	r2, r2, #4
  5c:	e2833004 	add	r3, r3, #4
  60:	e3520c01 	cmp	r2, #256	; 0x100
  64:	1afffff1 	bne	30 <sha_transform+0x30>
  68:	e5904000 	ldr	r4, [r0]
  6c:	e590b004 	ldr	fp, [r0, #4]
  70:	e5908008 	ldr	r8, [r0, #8]
  74:	e58d8004 	str	r8, [sp, #4]
  78:	e590200c 	ldr	r2, [r0, #12]
  7c:	e58d2008 	str	r2, [sp, #8]
  80:	e5903010 	ldr	r3, [r0, #16]
  84:	e58d300c 	str	r3, [sp, #12]
  88:	e28da05c 	add	sl, sp, #92	; 0x5c
  8c:	e1a06003 	mov	r6, r3
  90:	e1a07002 	mov	r7, r2
  94:	e1a05008 	mov	r5, r8
  98:	e1a0200b 	mov	r2, fp
  9c:	e1a03004 	mov	r3, r4
  a0:	e59f818c 	ldr	r8, [pc, #396]	; 234 <sha_transform+0x234>
  a4:	e1a09004 	mov	r9, r4
  a8:	ea000004 	b	c0 <sha_transform+0xc0>
  ac:	e1a06007 	mov	r6, r7
  b0:	e1a07005 	mov	r7, r5
  b4:	e1a05002 	mov	r5, r2
  b8:	e1a02003 	mov	r2, r3
  bc:	e1a0300c 	mov	r3, ip
  c0:	e086c008 	add	ip, r6, r8
  c4:	e08ccde3 	add	ip, ip, r3, ror #27
  c8:	e1c76002 	bic	r6, r7, r2
  cc:	e0054002 	and	r4, r5, r2
  d0:	e1864004 	orr	r4, r6, r4
  d4:	e08cc004 	add	ip, ip, r4
  d8:	e5b14004 	ldr	r4, [r1, #4]!
  dc:	e08cc004 	add	ip, ip, r4
  e0:	e1a02162 	ror	r2, r2, #2
  e4:	e151000a 	cmp	r1, sl
  e8:	1affffef 	bne	ac <sha_transform+0xac>
  ec:	e1a04009 	mov	r4, r9
  f0:	e28d105c 	add	r1, sp, #92	; 0x5c
  f4:	e28d80ac 	add	r8, sp, #172	; 0xac
  f8:	e59fa138 	ldr	sl, [pc, #312]	; 238 <sha_transform+0x238>
  fc:	e1a09007 	mov	r9, r7
 100:	ea000004 	b	118 <sha_transform+0x118>
 104:	e1a09005 	mov	r9, r5
 108:	e1a05002 	mov	r5, r2
 10c:	e1a02003 	mov	r2, r3
 110:	e1a0300c 	mov	r3, ip
 114:	e1a0c006 	mov	ip, r6
 118:	e089700a 	add	r7, r9, sl
 11c:	e0877dec 	add	r7, r7, ip, ror #27
 120:	e0226003 	eor	r6, r2, r3
 124:	e0266005 	eor	r6, r6, r5
 128:	e0877006 	add	r7, r7, r6
 12c:	e5b16004 	ldr	r6, [r1, #4]!
 130:	e0876006 	add	r6, r7, r6
 134:	e1a03163 	ror	r3, r3, #2
 138:	e1510008 	cmp	r1, r8
 13c:	1afffff0 	bne	104 <sha_transform+0x104>
 140:	e28d10ac 	add	r1, sp, #172	; 0xac
 144:	e28d70fc 	add	r7, sp, #252	; 0xfc
 148:	e59fa0ec 	ldr	sl, [pc, #236]	; 23c <sha_transform+0x23c>
 14c:	e1a09004 	mov	r9, r4
 150:	e1a08005 	mov	r8, r5
 154:	ea000004 	b	16c <sha_transform+0x16c>
 158:	e1a08002 	mov	r8, r2
 15c:	e1a02003 	mov	r2, r3
 160:	e1a0300c 	mov	r3, ip
 164:	e1a0c006 	mov	ip, r6
 168:	e1a06004 	mov	r6, r4
 16c:	e088400a 	add	r4, r8, sl
 170:	e0844de6 	add	r4, r4, r6, ror #27
 174:	e1828003 	orr	r8, r2, r3
 178:	e008800c 	and	r8, r8, ip
 17c:	e0025003 	and	r5, r2, r3
 180:	e1885005 	orr	r5, r8, r5
 184:	e0844005 	add	r4, r4, r5
 188:	e5b15004 	ldr	r5, [r1, #4]!
 18c:	e0844005 	add	r4, r4, r5
 190:	e1a0c16c 	ror	ip, ip, #2
 194:	e1510007 	cmp	r1, r7
 198:	1affffee 	bne	158 <sha_transform+0x158>
 19c:	e1a08004 	mov	r8, r4
 1a0:	e1a04009 	mov	r4, r9
 1a4:	e28d10fc 	add	r1, sp, #252	; 0xfc
 1a8:	e28d7f53 	add	r7, sp, #332	; 0x14c
 1ac:	e59fa08c 	ldr	sl, [pc, #140]	; 240 <sha_transform+0x240>
 1b0:	e1a09002 	mov	r9, r2
 1b4:	ea000004 	b	1cc <sha_transform+0x1cc>
 1b8:	e1a09003 	mov	r9, r3
 1bc:	e1a0300c 	mov	r3, ip
 1c0:	e1a0c006 	mov	ip, r6
 1c4:	e1a06008 	mov	r6, r8
 1c8:	e1a08005 	mov	r8, r5
 1cc:	e089500a 	add	r5, r9, sl
 1d0:	e0855de8 	add	r5, r5, r8, ror #27
 1d4:	e02c2006 	eor	r2, ip, r6
 1d8:	e0222003 	eor	r2, r2, r3
 1dc:	e0855002 	add	r5, r5, r2
 1e0:	e5b12004 	ldr	r2, [r1, #4]!
 1e4:	e0855002 	add	r5, r5, r2
 1e8:	e1a06166 	ror	r6, r6, #2
 1ec:	e1510007 	cmp	r1, r7
 1f0:	1afffff0 	bne	1b8 <sha_transform+0x1b8>
 1f4:	e0854004 	add	r4, r5, r4
 1f8:	e5804000 	str	r4, [r0]
 1fc:	e088800b 	add	r8, r8, fp
 200:	e5808004 	str	r8, [r0, #4]
 204:	e59d8004 	ldr	r8, [sp, #4]
 208:	e0866008 	add	r6, r6, r8
 20c:	e5806008 	str	r6, [r0, #8]
 210:	e59d2008 	ldr	r2, [sp, #8]
 214:	e08cc002 	add	ip, ip, r2
 218:	e580c00c 	str	ip, [r0, #12]
 21c:	e59d500c 	ldr	r5, [sp, #12]
 220:	e0833005 	add	r3, r3, r5
 224:	e5803010 	str	r3, [r0, #16]
 228:	e28dde15 	add	sp, sp, #336	; 0x150
 22c:	e8bd0ff0 	pop	{r4, r5, r6, r7, r8, r9, sl, fp}
 230:	e1a0f00e 	mov	pc, lr
 234:	5a827999 	.word	0x5a827999
 238:	6ed9eba1 	.word	0x6ed9eba1
 23c:	8f1bbcdc 	.word	0x8f1bbcdc
 240:	ca62c1d6 	.word	0xca62c1d6

00000244 <byte_reverse>:
 244:	e92d0030 	push	{r4, r5}
 248:	e1a01121 	lsr	r1, r1, #2
 24c:	e3510000 	cmp	r1, #0
 250:	da00000c 	ble	288 <byte_reverse+0x44>
 254:	e3a03000 	mov	r3, #0
 258:	e5d02000 	ldrb	r2, [r0]
 25c:	e5d0c001 	ldrb	ip, [r0, #1]
 260:	e5d04002 	ldrb	r4, [r0, #2]
 264:	e5d05003 	ldrb	r5, [r0, #3]
 268:	e5c05000 	strb	r5, [r0]
 26c:	e5c04001 	strb	r4, [r0, #1]
 270:	e5c0c002 	strb	ip, [r0, #2]
 274:	e5c02003 	strb	r2, [r0, #3]
 278:	e2800004 	add	r0, r0, #4
 27c:	e2833001 	add	r3, r3, #1
 280:	e1510003 	cmp	r1, r3
 284:	1afffff3 	bne	258 <byte_reverse+0x14>
 288:	e8bd0030 	pop	{r4, r5}
 28c:	e1a0f00e 	mov	pc, lr

00000290 <sha_init>:
 290:	e59f3030 	ldr	r3, [pc, #48]	; 2c8 <sha_init+0x38>
 294:	e5803000 	str	r3, [r0]
 298:	e59f302c 	ldr	r3, [pc, #44]	; 2cc <sha_init+0x3c>
 29c:	e5803004 	str	r3, [r0, #4]
 2a0:	e59f3028 	ldr	r3, [pc, #40]	; 2d0 <sha_init+0x40>
 2a4:	e5803008 	str	r3, [r0, #8]
 2a8:	e59f3024 	ldr	r3, [pc, #36]	; 2d4 <sha_init+0x44>
 2ac:	e580300c 	str	r3, [r0, #12]
 2b0:	e59f3020 	ldr	r3, [pc, #32]	; 2d8 <sha_init+0x48>
 2b4:	e5803010 	str	r3, [r0, #16]
 2b8:	e3a03000 	mov	r3, #0
 2bc:	e5803014 	str	r3, [r0, #20]
 2c0:	e5803018 	str	r3, [r0, #24]
 2c4:	e1a0f00e 	mov	pc, lr
 2c8:	67452301 	.word	0x67452301
 2cc:	efcdab89 	.word	0xefcdab89
 2d0:	98badcfe 	.word	0x98badcfe
 2d4:	10325476 	.word	0x10325476
 2d8:	c3d2e1f0 	.word	0xc3d2e1f0

000002dc <sha_update>:
 2dc:	e92d4ff8 	push	{r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
 2e0:	e1a08000 	mov	r8, r0
 2e4:	e1a09001 	mov	r9, r1
 2e8:	e1a0a002 	mov	sl, r2
 2ec:	e5902014 	ldr	r2, [r0, #20]
 2f0:	e1a0b00a 	mov	fp, sl
 2f4:	e082318a 	add	r3, r2, sl, lsl #3
 2f8:	e1520003 	cmp	r2, r3
 2fc:	85902018 	ldrhi	r2, [r0, #24]
 300:	82822001 	addhi	r2, r2, #1
 304:	85802018 	strhi	r2, [r0, #24]
 308:	e5803014 	str	r3, [r0, #20]
 30c:	e5903018 	ldr	r3, [r0, #24]
 310:	e0833eaa 	add	r3, r3, sl, lsr #29
 314:	e5803018 	str	r3, [r0, #24]
 318:	e35a003f 	cmp	sl, #63	; 0x3f
 31c:	da000016 	ble	37c <sha_update+0xa0>
 320:	e24ab040 	sub	fp, sl, #64	; 0x40
 324:	e1a0b32b 	lsr	fp, fp, #6
 328:	e2815040 	add	r5, r1, #64	; 0x40
 32c:	e085530b 	add	r5, r5, fp, lsl #6
 330:	e1a04001 	mov	r4, r1
 334:	e280601c 	add	r6, r0, #28
 338:	e3a07040 	mov	r7, #64	; 0x40
 33c:	e1a00006 	mov	r0, r6
 340:	e1a01004 	mov	r1, r4
 344:	e1a02007 	mov	r2, r7
 348:	ebfffffe 	bl	0 <memcpy>
 34c:	e1a00006 	mov	r0, r6
 350:	e1a01007 	mov	r1, r7
 354:	ebffffba 	bl	244 <byte_reverse>
 358:	e1a00008 	mov	r0, r8
 35c:	ebffff27 	bl	0 <sha_transform>
 360:	e2844040 	add	r4, r4, #64	; 0x40
 364:	e1540005 	cmp	r4, r5
 368:	1afffff3 	bne	33c <sha_update+0x60>
 36c:	e28b3001 	add	r3, fp, #1
 370:	e0899303 	add	r9, r9, r3, lsl #6
 374:	e24aa040 	sub	sl, sl, #64	; 0x40
 378:	e04aa30b 	sub	sl, sl, fp, lsl #6
 37c:	e288001c 	add	r0, r8, #28
 380:	e1a01009 	mov	r1, r9
 384:	e1a0200a 	mov	r2, sl
 388:	ebfffffe 	bl	0 <memcpy>
 38c:	e8bd8ff8 	pop	{r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}

00000390 <sha_final>:
 390:	e92d40f8 	push	{r3, r4, r5, r6, r7, lr}
 394:	e1a04000 	mov	r4, r0
 398:	e5906014 	ldr	r6, [r0, #20]
 39c:	e5907018 	ldr	r7, [r0, #24]
 3a0:	e1a021a6 	lsr	r2, r6, #3
 3a4:	e202203f 	and	r2, r2, #63	; 0x3f
 3a8:	e280501c 	add	r5, r0, #28
 3ac:	e3e0307f 	mvn	r3, #127	; 0x7f
 3b0:	e7c53002 	strb	r3, [r5, r2]
 3b4:	e2820001 	add	r0, r2, #1
 3b8:	e3500038 	cmp	r0, #56	; 0x38
 3bc:	da00000d 	ble	3f8 <sha_final+0x68>
 3c0:	e0850000 	add	r0, r5, r0
 3c4:	e3a01000 	mov	r1, #0
 3c8:	e262203f 	rsb	r2, r2, #63	; 0x3f
 3cc:	ebfffffe 	bl	0 <memset>
 3d0:	e1a00005 	mov	r0, r5
 3d4:	e3a01040 	mov	r1, #64	; 0x40
 3d8:	ebffff99 	bl	244 <byte_reverse>
 3dc:	e1a00004 	mov	r0, r4
 3e0:	ebffff06 	bl	0 <sha_transform>
 3e4:	e1a00005 	mov	r0, r5
 3e8:	e3a01000 	mov	r1, #0
 3ec:	e3a02038 	mov	r2, #56	; 0x38
 3f0:	ebfffffe 	bl	0 <memset>
 3f4:	ea000003 	b	408 <sha_final+0x78>
 3f8:	e0850000 	add	r0, r5, r0
 3fc:	e3a01000 	mov	r1, #0
 400:	e2622037 	rsb	r2, r2, #55	; 0x37
 404:	ebfffffe 	bl	0 <memset>
 408:	e1a00005 	mov	r0, r5
 40c:	e3a01040 	mov	r1, #64	; 0x40
 410:	ebffff8b 	bl	244 <byte_reverse>
 414:	e5847054 	str	r7, [r4, #84]	; 0x54
 418:	e5846058 	str	r6, [r4, #88]	; 0x58
 41c:	e1a00004 	mov	r0, r4
 420:	ebfffef6 	bl	0 <sha_transform>
 424:	e8bd80f8 	pop	{r3, r4, r5, r6, r7, pc}

00000428 <sha_stream>:
 428:	e92d40f0 	push	{r4, r5, r6, r7, lr}
 42c:	e24dda02 	sub	sp, sp, #8192	; 0x2000
 430:	e24dd004 	sub	sp, sp, #4
 434:	e1a07000 	mov	r7, r0
 438:	e1a05001 	mov	r5, r1
 43c:	ebfffffe 	bl	290 <sha_init>
 440:	e3a04001 	mov	r4, #1
 444:	e3a06a02 	mov	r6, #8192	; 0x2000
 448:	ea000002 	b	458 <sha_stream+0x30>
 44c:	e1a00007 	mov	r0, r7
 450:	e1a0100d 	mov	r1, sp
 454:	ebfffffe 	bl	2dc <sha_update>
 458:	e1a0000d 	mov	r0, sp
 45c:	e1a01004 	mov	r1, r4
 460:	e1a02006 	mov	r2, r6
 464:	e1a03005 	mov	r3, r5
 468:	ebfffffe 	bl	0 <fread>
 46c:	e2502000 	subs	r2, r0, #0
 470:	cafffff5 	bgt	44c <sha_stream+0x24>
 474:	e1a00007 	mov	r0, r7
 478:	ebfffffe 	bl	390 <sha_final>
 47c:	e28dd004 	add	sp, sp, #4
 480:	e28dda02 	add	sp, sp, #8192	; 0x2000
 484:	e8bd80f0 	pop	{r4, r5, r6, r7, pc}

00000488 <sha_print>:
 488:	e52de004 	push	{lr}		; (str lr, [sp, #-4]!)
 48c:	e24dd00c 	sub	sp, sp, #12
 490:	e5901000 	ldr	r1, [r0]
 494:	e5902004 	ldr	r2, [r0, #4]
 498:	e5903008 	ldr	r3, [r0, #8]
 49c:	e590c00c 	ldr	ip, [r0, #12]
 4a0:	e58dc000 	str	ip, [sp]
 4a4:	e5900010 	ldr	r0, [r0, #16]
 4a8:	e58d0004 	str	r0, [sp, #4]
 4ac:	e59f0008 	ldr	r0, [pc, #8]	; 4bc <sha_print+0x34>
 4b0:	ebfffffe 	bl	0 <printf>
 4b4:	e28dd00c 	add	sp, sp, #12
 4b8:	e8bd8000 	pop	{pc}
 4bc:	00000000 	.word	0x00000000
