
bitcnt_2.o:     file format elf32-littlearm


Disassembly of section .text:

00000000 <bitcount>:
   0:	e59f2054 	ldr	r2, [pc, #84]	; 5c <bitcount+0x5c>
   4:	e0002002 	and	r2, r0, r2
   8:	e59f3050 	ldr	r3, [pc, #80]	; 60 <bitcount+0x60>
   c:	e0003003 	and	r3, r0, r3
  10:	e08310a2 	add	r1, r3, r2, lsr #1
  14:	e59f2048 	ldr	r2, [pc, #72]	; 64 <bitcount+0x64>
  18:	e0012002 	and	r2, r1, r2
  1c:	e59f3044 	ldr	r3, [pc, #68]	; 68 <bitcount+0x68>
  20:	e0013003 	and	r3, r1, r3
  24:	e0831122 	add	r1, r3, r2, lsr #2
  28:	e59f203c 	ldr	r2, [pc, #60]	; 6c <bitcount+0x6c>
  2c:	e0012002 	and	r2, r1, r2
  30:	e59f3038 	ldr	r3, [pc, #56]	; 70 <bitcount+0x70>
  34:	e0013003 	and	r3, r1, r3
  38:	e0832222 	add	r2, r3, r2, lsr #4
  3c:	e3c238ff 	bic	r3, r2, #16711680	; 0xff0000
  40:	e3c224ff 	bic	r2, r2, #-16777216	; 0xff000000
  44:	e3c22cff 	bic	r2, r2, #65280	; 0xff00
  48:	e0823423 	add	r3, r2, r3, lsr #8
  4c:	e1a00803 	lsl	r0, r3, #16
  50:	e1a03823 	lsr	r3, r3, #16
  54:	e0830820 	add	r0, r3, r0, lsr #16
  58:	e1a0f00e 	mov	pc, lr
  5c:	aaaaaaaa 	.word	0xaaaaaaaa
  60:	55555555 	.word	0x55555555
  64:	cccccccc 	.word	0xcccccccc
  68:	33333333 	.word	0x33333333
  6c:	f0f0f0f0 	.word	0xf0f0f0f0
  70:	0f0f0f0f 	.word	0x0f0f0f0f
