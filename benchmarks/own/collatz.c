#include <stdio.h>
#include <stdlib.h>
//#define DEBUG

unsigned int collatz(unsigned int an)
{
    unsigned int iters=1;
  
    while (an != 1) {        
        if (an & 1) { // even number, a[n] = a[n-1]*3 + 1
            an *= 3;
            an += 1;
        }
        else // odd number, a[n] = a[n-1]/2
            an >>= 1;

        iters++;
    }

    return iters;
}

int main(int argc, char *argv[])
{
    int i, n;
    unsigned int total_iters = 0;

    if (argc != 2) return 1;
    n = strtol(argv[1], NULL, 10);

    printf("checking the collatz conjecture for [1,%d]...\n", n);
    for (i=1; i<n; i++) {
        total_iters += collatz(i);
    }
    printf("conjecture is ok for [1,%d], total iterations: %d\n", n, total_iters);
    return 0;
}
