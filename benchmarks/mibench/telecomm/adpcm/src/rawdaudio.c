/* testd - Test adpcm decoder */

#include "adpcm.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

struct adpcm_state state;

#define NSAMPLES 1000

char	abuf[NSAMPLES/2];
short	sbuf[NSAMPLES];

main(int argc, char *argv[]) {
    int n;
    int fdr, fdw;
    fdr = open(argv[1], O_RDONLY);
    if (fdr < 0) {
        printf("error couldn't open file for reading!\n");
        exit(1);
    }
    fdw = open(argv[2], O_WRONLY | O_CREAT, 0644);
    if (fdw < 0) {
        printf("error couldn't open file for writing!\n");
        exit(2);
    }

    while(1) {
	n = read(fdr, abuf, NSAMPLES/2);
	if ( n < 0 ) {
	    perror("input file");
	    exit(1);
	}
	if ( n == 0 ) break;
	adpcm_decoder(abuf, sbuf, n*2, &state);
	write(fdw, sbuf, n*4);
    }
    //fprintf(stderr, "Final valprev=%d, index=%d\n",
	//    state.valprev, state.index);
    exit(0);
}
