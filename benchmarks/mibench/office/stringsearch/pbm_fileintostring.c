/* +++Date last modified: 05-Jul-1997 */

/*
**        A Pratt-Boyer-Moore string search, written by Jerry Coffin
**  sometime or other in 1991.  Removed from original program, and
**  (incorrectly) rewritten for separate, generic use in early 1992.
**  Corrected with help from Thad Smith, late March and early
**  April 1992...hopefully it's correct this time. Revised by Bob Stout.
**
**  This is hereby placed in the Public Domain by its author.
**
**  10/21/93 rdg  Fixed bug found by Jeff Dunlop
*/

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "search.h"

static size_t table[UCHAR_MAX + 1];
static size_t len;
static char *findme;

/*
**  Call this with the string to locate to initialize the table
*/

void init_search(const char *string)
{
      size_t i;

      len = strlen(string);
      for (i = 0; i <= UCHAR_MAX; i++)                      /* rdg 10/93 */
            table[i] = len;
      for (i = 0; i < len; i++)
            table[(unsigned char)string[i]] = len - i - 1;
      findme = (char *)string;
}

/*
**  Call this with a buffer to search
*/

char *strsearch(const char *string)
{
      register size_t shift;
      register size_t pos = len - 1;
      char *here;
      size_t limit=strlen(string);

      while (pos < limit)
      {
            while( pos < limit &&
                  (shift = table[(unsigned char)string[pos]]) > 0)
            {
                  pos += shift;
            }
            if (0 == shift)
            {
                  if (0 == strncmp(findme,
                        here = (char *)&string[pos-len+1], len))
                  {
                        return(here);
                  }
                  else  pos++;
            }
      }
      return NULL;
}

#include <stdio.h>

int main(int argc, char *argv[])
{
    FILE *f;
    int flen;
    char *here;
    char *find_strings[500];
    char *buffer;
    char *line;
    int i;


    /************************ load patterns into array *****************/
    f = fopen(argv[1], "r");
    if (f < 0) {
        printf("couldn't open pattern file!\n");
        exit(1);
    }

    memset(find_strings, 0, sizeof(find_strings));
    for (i=0; getline(&find_strings[i], &flen, f) != -1; i++) {
        flen = strlen(find_strings[i]);
        if (find_strings[i][flen-1] == '\n') {
            find_strings[i][flen-1] = '\0';
            flen--;
        }
        if (find_strings[i][flen-1] == '\r') {
            find_strings[i][flen-1] = '\0';
            flen--;
        }
        //printf("read the following line: \"%s\"\n", find_strings[i]);
    }
    find_strings[i] = NULL;
    fclose(f);

    /*************** load contents of search text file into a single string ***************/
    f = fopen(argv[2], "rb");
    if (f < 0) {
        printf("couldn't open search text file!\n");
        exit(2);
    }
    fseek(f, 0, SEEK_END);
    flen = ftell(f);
    fseek(f, 0, SEEK_SET);
    buffer = (char*)malloc(flen+1);
    if (buffer == NULL) {
        printf("couldn't allocate %d bytes of memory!\n", flen);
        exit(1);
    }
    fread(buffer, 1, flen, f);
    fclose(f);
    buffer[flen] = '\0';

      for (i = 0; find_strings[i]; i++)
      {
            //printf("searching for \"%s\" in large string... ", find_strings[i]);
#if SEARCH_PBM
            init_search(find_strings[i]);
            here = strsearch(buffer);
#elif SEARCH_BMH
            bmh_init(find_strings[i]);
            here = bmh_search(buffer, strlen(buffer));
#else
    #error "define either SEARCH_PBM or SEARCH_BMH!"
#endif

#if 0
            if (here)
                printf("\"%s\" FOUND!\n", find_strings[i]);
#endif
            //else
            //    printf("not found\n");
            //printf("\"%s\" is%s in \"%s\"", find_strings[i],
            //      here ? "" : " not", search_strings[i]);
            //if (here)
            //      printf(" [\"%s\"]", here);
            //putchar('\n');
      }

      return 0;
}

