#!/bin/bash
for TEST in *.c
do
	echo -n === Performing test for file "$TEST" === --
    TESTNUM=${TEST:0:2}
    if [[ $TESTNUM == "22" || $TESTNUM == "23" || $TESTNUM == "24" || $TESTNUM == "49" || $TESTNUM == "55" || $TESTNUM == "30" || $TESTNUM == "34" || $TESTNUM == "46" ]]; then
        echo IGNORED
        continue
    fi
	../../tcc -o test $TEST > /dev/null
    ./test > test.out
    TESTEXPECTED=`basename $TEST .c`.expect
    diff test.out $TESTEXPECTED -q > /dev/null
    if [ $? -ne 0 ]; then
        echo -e "\e[00;31mFAILED\e[00m"
    else
        echo OK
    fi
done
