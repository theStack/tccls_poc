#ifndef _TCCLS_H
#define _TCCLS_H

void LS_ClearLiveIntervals(void);
void LS_AddLiveInterval(int vreg, int start, int end);
void LS_RegisterAllocation(void);

#endif
